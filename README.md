# Evalorm #

### General ###

* Experimental project for evaluation forms
* Goals:
  * Store detailed information for a person after each interview
  * Store detailed information for a person, starting from interviews and after each evaluation
  * Collect answers and wordings for characteristics, so interviewers can save time for filling interview/evaluation forms
  * Build skills standards: available for everyone, collected in one place, with possibility to compare with current person's skills 
  * Build growth plans: available for everyone, collected in one place, with possibility to build a personal growth plan

### How do I get set up? ###

* There are two separate projects: client and server
* Client is based on Facebook's create-react-app
* Server side is sbt + Play Framework 2.6
* MongoDB data storage
* Deployment will be done with docker-compose later

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* mak.spectrum@gmail.com