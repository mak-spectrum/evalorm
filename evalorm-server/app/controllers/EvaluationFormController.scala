package controllers

import javax.inject.{Inject, Singleton}

import com.google.inject.name.Named
import io.circe.syntax.EncoderOps
import io.evalorm.cfg.service.EvaluationFormConfigurationService
import io.evalorm.form.model.EvaluationFormDataRecord
import io.evalorm.form.service.EvaluationFormDataRecordService
import play.api.http.ContentTypes
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents, Request}
import utility.{BaseControllerConfiguration, CirceSupport}

/**
 * This controller creates an `Action` to handle REST API requests for `EvaluationFormConfiguration` and
 * `EvaluationFormRecord`.
 */
@Singleton
class EvaluationFormController @Inject()(cc: ControllerComponents,
                                         service: EvaluationFormDataRecordService,
                                         cfgService: EvaluationFormConfigurationService) extends AbstractController(cc)
    with BaseControllerConfiguration with CirceSupport {

  def readConfiguration(keyCode: String): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      cfgService.readConfiguration(keyCode) match {
        case Some(form) => Ok(form.asJson.noSpaces).as(ContentTypes.JSON)
        case None => NotFound
      }
    }
  }

  def listConfigurations() = Action { implicit request: Request[AnyContent] =>
    val configurations = cfgService.listConfigurations()
    Ok(configurations.asJson.noSpaces).as(ContentTypes.JSON)
  }

  def createFormDataRecord: Action[EvaluationFormDataRecord] = Action(circe.tolerantJson[EvaluationFormDataRecord]) {
    implicit request: Request[EvaluationFormDataRecord]  => {
      val key = service.persistObject(None, request.body)
      Ok(Map("uuid" -> key.uuid).asJson.noSpaces).as(ContentTypes.JSON)
    }
  }

  def readFormDataRecord(uuid: String): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      service.fetchObject(uuid) match {
        case Some(record) => Ok(record.asJson.noSpaces).as(ContentTypes.JSON)
        case None => NotFound
      }
    }
  }

  def listFormDataRecords(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      Ok(service.fetchList(None).asJson.noSpaces).as(ContentTypes.JSON)
    }
  }

  def updateFormDataRecord(uuid: String): Action[EvaluationFormDataRecord] = Action(circe.tolerantJson[EvaluationFormDataRecord]) {
    implicit request: Request[EvaluationFormDataRecord]  => {
      val key = service.persistObject(Some(uuid), request.body)

      Ok(Map("uuid" -> key.uuid).asJson.noSpaces).as(ContentTypes.JSON)
    }
  }

  def deleteFormDataRecord(uuid: String): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      if (service.deleteObject(uuid)) {
        NoContent
      } else {
        NotFound
      }
    }
  }
}
