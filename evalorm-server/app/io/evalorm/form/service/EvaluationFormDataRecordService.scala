package io.evalorm.form.service

import com.google.inject.ImplementedBy
import io.evalorm.common.configuration.SystemConfiguration
import io.evalorm.common.persister.IdentifiableEntityPersister
import io.evalorm.common.persister.model.{IdentifiableEntityFilter, IdentifiableEntityKey}
import io.evalorm.form.model.EvaluationFormDataRecord


@ImplementedBy(classOf[EvaluationFormDataRecordServiceImpl])
trait EvaluationFormDataRecordService {

  def fetchList(filter: Option[IdentifiableEntityFilter]): Seq[IdentifiableEntityKey]

  def fetchObject(uuid: String): Option[EvaluationFormDataRecord]

  def persistObject(uuid: Option[String], obj: EvaluationFormDataRecord): IdentifiableEntityKey

  def deleteObject(uuid: String): Boolean

}

class EvaluationFormDataRecordServiceImpl extends EvaluationFormDataRecordService {

  lazy val persister: IdentifiableEntityPersister[EvaluationFormDataRecord] =
    IdentifiableEntityPersister.createPersister[EvaluationFormDataRecord](
      SystemConfiguration.getDataStoreConfig, "EvaluationFormDataRecord")

  override def fetchList(filter: Option[IdentifiableEntityFilter]): Seq[IdentifiableEntityKey] =
    persister.fetchList(filter)

  override def fetchObject(uuid: String): Option[EvaluationFormDataRecord] =
    persister.fetchObject(uuid)

  override def persistObject(uuid: Option[String], obj: EvaluationFormDataRecord): IdentifiableEntityKey =
    persister.persistObject(uuid, obj)

  override def deleteObject(uuid: String): Boolean =
    persister.deleteObject(uuid)

}
