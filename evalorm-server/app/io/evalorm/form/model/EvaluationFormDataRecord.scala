package io.evalorm.form.model

import io.evalorm.cfg.model.FieldType
import io.evalorm.common.persister.model.IdentifiableEntity


case class EvaluationFormDataRecord(
                                 var uuid: Option[String],
                                 keyCode: String,
                                 label: String,
                                 candidateName: String,
                                 interviewedBy: String,
                                 interviewDate: String,
                                 sections: Seq[EvaluationSectionData]) extends IdentifiableEntity {

  override def getUuid: Option[String] = uuid

  override def setUuid(uuid: String): Unit = this.uuid = Some(uuid)

  // TODO: Stupid realization. Need changes.
  override def getAlias: String = {
    label + " / " + candidateName + " / " + interviewDate.substring(0, interviewDate.indexOf('T'))}
}

case class EvaluationSectionData(keyCode: String, label: String, fields: Seq[EvaluationFieldData])

case class EvaluationFieldData(keyCode: String, label: String, fieldType: FieldType, value: EvaluationFieldValue)

trait EvaluationFieldValue {

  def stringFieldValue: String

}

case class FeedbackFieldValue(feedback: String, score: Int) extends EvaluationFieldValue {

  override def stringFieldValue = s"$feedback - [$score/100]"

}

case class BooleanFieldValue(value: Boolean) extends EvaluationFieldValue {

  override def stringFieldValue: String = if (value) "Yes" else "No"

}

case class StringFieldValue(value: String) extends EvaluationFieldValue {

  override def stringFieldValue: String = value

}

case class DropDownFieldValue(selected: String, options: Seq[DropDownOptionValue]) extends EvaluationFieldValue {
  override def stringFieldValue = selected
}

case class DropDownOptionValue(label: String, value: String) extends EvaluationFieldValue {
  override def stringFieldValue = s"$label: $value"
}

