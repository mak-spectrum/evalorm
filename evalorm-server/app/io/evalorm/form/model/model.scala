package io.evalorm.form

import com.mongodb.DBObject
import com.mongodb.casbah.Implicits
import com.mongodb.casbah.commons.MongoDBObject
import io.circe.Decoder.Result
import io.circe.syntax.EncoderOps
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json, JsonObject}
import io.evalorm.cfg.model.FieldType
import io.evalorm.common.exception.DataTransformationException
import io.evalorm.common.persister.CodecType.MongoDBCodec
import io.evalorm.common.persister.mongodb.MongoDBConstants
import io.evalorm.common.persister.{Codec, CodecType}
import org.bson.types.ObjectId

import scala.collection.immutable.ListMap

package object model {

  /*
   * DB codecs
   */
  implicit val evaluationFormDataRecordCodec: Codec[EvaluationFormDataRecord] = new Codec[EvaluationFormDataRecord] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): EvaluationFormDataRecord = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        EvaluationFormDataRecord(
          rawObject.getAs[ObjectId](MongoDBConstants.UuidColumnName).map(_.toString),
          rawObject.getAs[String]("keyCode").get,
          rawObject.getAs[String]("label").get,
          rawObject.getAs[String]("candidateName").get,
          rawObject.getAs[String]("interviewedBy").get,
          rawObject.getAs[String]("interviewDate").get,
          rawObject.getAs[Seq[DBObject]]("sections").get.map(subObj => evaluationSectionDataCodec.decode(subObj))
        )
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](obj: EvaluationFormDataRecord)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {

        val mongoRecord = MongoDBObject(
          "keyCode" -> obj.keyCode,
          "label" -> obj.label,
          "candidateName" -> obj.candidateName,
          "interviewedBy" -> obj.interviewedBy,
          "interviewDate" -> obj.interviewDate,
          "sections" -> obj.sections.map(section => evaluationSectionDataCodec.encode[DBObject](section))
        )

        obj.uuid.foreach(uuid => mongoRecord.put(MongoDBConstants.UuidColumnName, new ObjectId(uuid)))

        mongoRecord.asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }
  }

  implicit val evaluationSectionDataCodec: Codec[EvaluationSectionData] = new Codec[EvaluationSectionData] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): EvaluationSectionData = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        EvaluationSectionData(
          rawObject.getAs[String]("keyCode").get,
          rawObject.getAs[String]("label").get,
          rawObject.getAs[Seq[DBObject]]("fields").get.map(subObj => evaluationFieldDataCodec.decode(subObj))
        )
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](obj: EvaluationSectionData)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {
        val mongoRecord = MongoDBObject(
          "keyCode" -> obj.keyCode,
          "label" -> obj.label,
          "fields" -> obj.fields.map(field => evaluationFieldDataCodec.encode[DBObject](field))
        )
        mongoRecord.asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }
  }

  implicit val evaluationFieldDataCodec: Codec[EvaluationFieldData] = new Codec[EvaluationFieldData] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): EvaluationFieldData = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        val keyCode = rawObject.getAs[String]("keyCode").get
        val fieldType: FieldType = FieldType.withName(rawObject.getAs[String]("fieldType").get)

        EvaluationFieldData(
          keyCode,
          rawObject.getAs[String]("label").get,
          fieldType,
          rawObject.getAs[DBObject]("value").map(fieldValue => evaluationFieldValueCodec(fieldType).decode(fieldValue))
            .getOrElse(throw new DataTransformationException(s"Cannot find value of field [$keyCode]"))
        )
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](obj: EvaluationFieldData)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {

        val mongoRecord = MongoDBObject(
          "keyCode" -> obj.keyCode,
          "label" -> obj.label,
          "fieldType" -> obj.fieldType.entryName,
          "value" -> evaluationFieldValueCodec(obj.fieldType).encode[DBObject](obj.value)
        )

        mongoRecord.asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

  }

  private def evaluationFieldValueCodec(fieldType: FieldType): Codec[EvaluationFieldValue] =
    fieldType match {
      case FieldType.Feedback => feedbackFieldValueCodec.asInstanceOf[Codec[EvaluationFieldValue]]
      case FieldType.DropDown => dropDownFieldValueCodec.asInstanceOf[Codec[EvaluationFieldValue]]
      case FieldType.Boolean => booleanFieldValueCodec.asInstanceOf[Codec[EvaluationFieldValue]]
    }

  private val feedbackFieldValueCodec: Codec[FeedbackFieldValue] = new Codec[FeedbackFieldValue] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): FeedbackFieldValue = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        FeedbackFieldValue(rawObject.getAs[String]("feedback").get, rawObject.getAs[Int]("score").get)
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](fieldValue: FeedbackFieldValue)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {
        MongoDBObject(
          "feedback" -> fieldValue.feedback,
          "score" -> fieldValue.score
        ).asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

  }

  private val dropDownFieldValueCodec: Codec[DropDownFieldValue] = new Codec[DropDownFieldValue] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): DropDownFieldValue = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        DropDownFieldValue(
          rawObject.getAs[String]("selected").get,
          rawObject.getAs[Seq[DBObject]]("options").get.map(subObj => dropDownFieldOptionValueCodec.decode(subObj))
        )
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](fieldValue: DropDownFieldValue)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {
        MongoDBObject(
          "options" -> fieldValue.options.map(option => dropDownFieldOptionValueCodec.encode[DBObject](option)),
          "selected" -> fieldValue.selected
        ).asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

  }

  private val dropDownFieldOptionValueCodec: Codec[DropDownOptionValue] = new Codec[DropDownOptionValue] {
    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): DropDownOptionValue = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        DropDownOptionValue(
          rawObject.getAs[String]("label").get,
          rawObject.getAs[String]("value").get
        )
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](option: DropDownOptionValue)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {
        MongoDBObject(
          "label" -> option.label,
          "value" -> option.value
        ).asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }
  }

  private val stringFieldValueCodec: Codec[StringFieldValue] = new Codec[StringFieldValue] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): StringFieldValue = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        StringFieldValue(rawObject.getAs[String]("value").get)
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](fieldValue: StringFieldValue)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {
        MongoDBObject(
          "value" -> fieldValue.value
        ).asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

  }

  private val booleanFieldValueCodec: Codec[BooleanFieldValue] = new Codec[BooleanFieldValue] {

    override def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): BooleanFieldValue = {
      if (codecType == MongoDBCodec) {
        val rawObject = Implicits.wrapDBObj(rawForm.asInstanceOf[DBObject])
        BooleanFieldValue(rawObject.getAs[Boolean]("value").get)
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

    override def encode[RawType](fieldValue: BooleanFieldValue)(implicit codecType: CodecType): RawType = {
      if (codecType == MongoDBCodec) {
        MongoDBObject(
          "value" -> fieldValue.value
        ).asInstanceOf[RawType]
      } else {
        throw new DataTransformationException(s"Cannot handle the provided codec type [${codecType.entryName}]")
      }
    }

  }



  /*
   * Circe Decodes/Encoders
   */
  implicit val evaluationFormRecordDecoder: Decoder[EvaluationFormDataRecord] = new Decoder[EvaluationFormDataRecord] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFormDataRecord] =
      for {
        uuid <- c.downField("uuid").as[Option[String]]
        keyCode <- c.downField("keyCode").as[String]
        label <- c.downField("label").as[String]
        candidateName <- c.downField("candidateName").as[String]
        interviewedBy <- c.downField("interviewedBy").as[String]
        interviewDate <- c.downField("interviewDate").as[String]
        sections <- c.downField("sections").as[Seq[EvaluationSectionData]]
      } yield {
        EvaluationFormDataRecord(uuid, keyCode, label, candidateName, interviewedBy, interviewDate, sections)
      }
  }

  implicit val evaluationSectionDataDecoder: Decoder[EvaluationSectionData] = new Decoder[EvaluationSectionData] {
    final def apply(c: HCursor): Decoder.Result[EvaluationSectionData] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        label <- c.downField("label").as[String]
        fields <- c.downField("fields").as[Seq[EvaluationFieldData]]
      } yield {
        EvaluationSectionData(keyCode, label, fields)
      }
  }

  implicit val evaluationFieldDataDecoder: Decoder[EvaluationFieldData] = new Decoder[EvaluationFieldData] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldData] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        label <- c.downField("label").as[String]
        fieldType <- c.downField("fieldType").as[FieldType]
        value <- decodeEvaluationFieldValue(fieldType).tryDecode(c.downField("value"))
      } yield {
        EvaluationFieldData(keyCode, label, fieldType, value)
      }
  }

  private def decodeEvaluationFieldValue(fieldType: FieldType): Decoder[EvaluationFieldValue] =
    fieldType match {
      case FieldType.Feedback => feedbackFieldValueDecoder
      case FieldType.DropDown => dropDownFieldValueDecoder
      case FieldType.Boolean => booleanFieldValueDecoder
    }

  private val feedbackFieldValueDecoder: Decoder[EvaluationFieldValue] = new Decoder[EvaluationFieldValue] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldValue] =
      for {
        feedback <- c.downField("feedback").as[String]
        score <- c.downField("score").as[Int]
      } yield {
        FeedbackFieldValue(feedback, score)
      }
  }

  private val dropDownFieldValueDecoder: Decoder[EvaluationFieldValue] = new Decoder[EvaluationFieldValue] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldValue] =
      for {
        options <- c.downField("options").as[Seq[DropDownOptionValue]]
        selected <- c.downField("selected").as[String]
      } yield {
        DropDownFieldValue(selected, options)
      }
  }

  implicit val dropDownOptionDecoder: Decoder[DropDownOptionValue] = new Decoder[DropDownOptionValue] {
    override def apply(c: HCursor): Decoder.Result[DropDownOptionValue] =
      for {
        label <- c.downField("label").as[String]
        value <- c.downField("value").as[String]
      } yield {
        DropDownOptionValue(label, value)
      }
  }

  private val booleanFieldValueDecoder: Decoder[EvaluationFieldValue] = new Decoder[EvaluationFieldValue] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldValue] =
      for {
        value <- c.as[Boolean]
      } yield {
        BooleanFieldValue(value)
      }
  }



  implicit val evaluationFormRecordEncoder: Encoder[EvaluationFormDataRecord] = new Encoder[EvaluationFormDataRecord] {
    override def apply(record: EvaluationFormDataRecord): Json = {
      Json.fromJsonObject(JsonObject.fromMap(
        (if (record.uuid.isDefined) {
          ListMap[String, Json]("uuid" -> Json.fromString(record.uuid.get))
        } else {
          ListMap[String, Json]()
        }) ++ ListMap[String, Json](
          "keyCode" -> Json.fromString(record.keyCode),
          "label" -> Json.fromString(record.label),
          "candidateName" -> Json.fromString(record.candidateName),
          "interviewedBy" -> Json.fromString(record.interviewedBy),
          "interviewDate" -> Json.fromString(record.interviewDate),
          "sections" -> Json.fromValues(record.sections.map(evaluationSectionDataEncoder(_)))
        )
      ))
    }
  }

  implicit val evaluationSectionDataEncoder: Encoder[EvaluationSectionData] = new Encoder[EvaluationSectionData] {
    override def apply(s: EvaluationSectionData): Json = Json.obj(
      "keyCode" -> Json.fromString(s.keyCode),
      "label" -> Json.fromString(s.label),
      "fields" -> Json.fromValues(s.fields.map(evaluationFieldDataEncoder(_)))
    )
  }

  implicit val evaluationFieldDataEncoder: Encoder[EvaluationFieldData] = new Encoder[EvaluationFieldData] {
    override def apply(f: EvaluationFieldData): Json = Json.obj(
      "keyCode" -> Json.fromString(f.keyCode),
      "label" -> Json.fromString(f.label),
      "fieldType" -> f.fieldType.asJson,
      "value" -> encodeEvaluationFieldValue(f.value)
    )
  }

  private def encodeEvaluationFieldValue(fieldValue: EvaluationFieldValue): Json =
    fieldValue match {
      case feedbackValue: FeedbackFieldValue => feedbackFieldValueEncoder(feedbackValue)
      case stringValue: StringFieldValue => stringFieldValueEncoder(stringValue)
      case booleanValue: BooleanFieldValue => booleanFieldValueEncoder(booleanValue)
      case dropDownValue: DropDownFieldValue => dropDownFieldValueEncoder(dropDownValue)
    }

  private val feedbackFieldValueEncoder: Encoder[FeedbackFieldValue] = new Encoder[FeedbackFieldValue] {
    override def apply(fieldValue: FeedbackFieldValue): Json = Json.obj(
      "feedback" -> Json.fromString(fieldValue.feedback),
      "score" -> Json.fromInt(fieldValue.score)
    )
  }

  private val stringFieldValueEncoder: Encoder[StringFieldValue] = new Encoder[StringFieldValue] {
    override def apply(fieldValue: StringFieldValue): Json = Json.fromString(fieldValue.value)
}

  private val booleanFieldValueEncoder: Encoder[BooleanFieldValue] = new Encoder[BooleanFieldValue] {
    override def apply(fieldValue: BooleanFieldValue): Json = Json.fromBoolean(fieldValue.value)
  }

  private val dropDownFieldValueEncoder: Encoder[DropDownFieldValue] = new Encoder[DropDownFieldValue] {
    override def apply(fieldValue: DropDownFieldValue): Json = Json.obj(
      "selected" -> Json.fromString(fieldValue.selected),
      "options" -> Json.fromValues(fieldValue.options.map(dropDownOptionEncoder(_)))
    )
  }

  private val dropDownOptionEncoder: Encoder[DropDownOptionValue] = new Encoder[DropDownOptionValue] {
    override def apply(optionValue: DropDownOptionValue): Json = Json.obj(
      "label" -> Json.fromString(optionValue.label),
      "value" -> Json.fromString(optionValue.value)
    )
  }
}
