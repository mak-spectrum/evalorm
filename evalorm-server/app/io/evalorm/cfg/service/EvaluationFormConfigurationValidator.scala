package io.evalorm.cfg.service

import io.evalorm.cfg.model.{EvaluationFieldConfiguration, EvaluationFormConfiguration, EvaluationSectionConfiguration}
import io.evalorm.common.validation.ValidationContext

import scala.collection.immutable.HashSet

object EvaluationFormConfigurationValidator {

  def validateForm(form: EvaluationFormConfiguration, validationFile: String): ValidationContext = {
    val result = new ValidationContext()

    if (form.keyCode == null || form.keyCode.isEmpty) {
      result.addError(s"File [$validationFile]. Form keyCode is missed.")

    } else if (form.keyCode != cutFileNameWithoutExtension(validationFile)) {
      result.addError(s"File name [$validationFile] and form keyCode [${form.keyCode}] don't match.")

    } else if (form.sections == null || form.sections.isEmpty) {
      result.addError(s"Form [${form.keyCode}]. Form sections are missed.")

    } else {

      this.checkNonUniqueSectionKeyCodes(form.sections)
        .foreach(nonUniqueKeyCode =>
          result.addError(s"Form [${form.keyCode}]. Section keyCode [$nonUniqueKeyCode] is not unique.")
        )

      form.sections
        .flatMap(section => this.validateSection(section).getMessages)
        .foreach(sectionError =>
          result.addError(s"Form [${form.keyCode}]. ${sectionError.content}")
        )
    }

    result
  }

  private def cutFileNameWithoutExtension(fileName: String): String = {
    val extensionStart = fileName.lastIndexOf('.')

    if (extensionStart > 0) {
      fileName.substring(0, extensionStart)
    } else {
      fileName
    }
  }

  private def checkNonUniqueSectionKeyCodes(sections: Seq[EvaluationSectionConfiguration]): Seq[String] = {
    var hashSet = new HashSet[String]()

    for (section1Idx <- sections.indices) {
      for (section2Idx <- sections.indices) {
        if (section1Idx != section2Idx && sections(section1Idx).keyCode == sections(section2Idx).keyCode) {
          hashSet += sections(section1Idx).keyCode
        }
      }
    }

    hashSet.toSeq
  }

  private def validateSection(section: EvaluationSectionConfiguration): ValidationContext = {
    val result = new ValidationContext()

    if (section.keyCode == null || section.keyCode.isEmpty) {
      result.addError("Section keyCode is missed.")

    } else if (section.fields == null || section.fields.isEmpty) {
      result.addError(s"Section [${section.keyCode}] fields are missed.")

    } else {
      this.checkNonUniqueFieldKeyCodes(section.fields)
        .foreach(nonUniqueKeyCode =>
          result.addError(s"Section [${section.keyCode}]. Field keyCode [$nonUniqueKeyCode] is not unique.")
        )

      section.fields
        .flatMap(field => this.validateField(field).getMessages)
        .foreach(
          fieldError => result.addError(s"Section [${section.keyCode}]. ${fieldError.content}")
        )
    }

    result
  }

  private def checkNonUniqueFieldKeyCodes(fields: Seq[EvaluationFieldConfiguration]): Seq[String] = {
    var hashSet = new HashSet[String]()

    for (field1Idx <- fields.indices) {
      for (field2Idx <- fields.indices) {
        if (field1Idx != field2Idx && fields(field1Idx).keyCode == fields(field2Idx).keyCode) {
          hashSet += fields(field1Idx).keyCode
        }
      }
    }

    hashSet.toSeq
  }

  private def validateField(field: EvaluationFieldConfiguration): ValidationContext = {
    val result = new ValidationContext()

    if (field.keyCode == null || field.keyCode.isEmpty) {
      result.addError("Field keyCode is missed.")
    }

    result
  }

}
