package io.evalorm.cfg.service

import com.typesafe.scalalogging.LazyLogging
import io.circe.Error
import io.circe.parser.decode
import io.evalorm.cfg.model.EvaluationFormConfiguration
import io.evalorm.common.validation.ValidationContext

import scala.io.Source

object EvaluationFormConfigurationRegistry extends LazyLogging {

  lazy val FormConfigurationsList: Map[String, EvaluationFormConfiguration] =
    Source.fromURL(this.getClass.getResource("/form/forms.cfg"))
      .getLines()
      .map(formFileName =>
        decodeEFConfigurationFromResources(formFileName) match {
          case Left(_) =>
            logger.error(s"Error decoding evaluation form configuration file [$formFileName]")
            (None, None)
          case Right(form) =>

            val validationContext = EvaluationFormConfigurationValidator.validateForm(form, formFileName)
            if (validationContext.isEmpty) {
              (Some(form.keyCode), Some(form))
            } else {
              val overallMessage = this.linkMessages(validationContext)
              logger.error(s"Error validating evaluation form from configuration file [$formFileName]:\n$overallMessage")
              (None, None)
            }
        })
      .filter(tuple => tuple._1.isDefined && tuple._2.isDefined)
      .map(tuple => (tuple._1.get, tuple._2.get))
      .toMap


  private def linkMessages(validationContext: ValidationContext): String  = {
    val overallValidationMessage = StringBuilder.newBuilder
    for (message <- validationContext.getMessages) {
      if (overallValidationMessage.isEmpty) {
        overallValidationMessage.append(message.content)
      } else {
        overallValidationMessage.append("\n").append(message.content)
      }
    }
    overallValidationMessage.toString()
  }

  private def decodeEFConfigurationFromResources(formFileName: String): Either[Error, EvaluationFormConfiguration] =
    decode[EvaluationFormConfiguration](
      Source.fromURL(this.getClass.getResource(s"/form/$formFileName")).mkString
    )

}