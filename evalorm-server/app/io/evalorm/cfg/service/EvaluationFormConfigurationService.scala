package io.evalorm.cfg.service

import com.google.inject.ImplementedBy
import io.evalorm.cfg.model.EvaluationFormConfiguration

@ImplementedBy(classOf[EvaluationFormConfigurationServiceImpl])
trait EvaluationFormConfigurationService {
  def readConfiguration(keyCode: String): Option[EvaluationFormConfiguration]

  def listConfigurations(): Seq[Map[String, String]]
}

class EvaluationFormConfigurationServiceImpl extends EvaluationFormConfigurationService {

  // TODO: add CRUD tests
  override def readConfiguration(keyCode: String): Option[EvaluationFormConfiguration] =
    EvaluationFormConfigurationRegistry.FormConfigurationsList.get(keyCode)

  override def listConfigurations(): Seq[Map[String, String]] = {
    EvaluationFormConfigurationRegistry.FormConfigurationsList.values
      .map(configuration => Map("keyCode" -> configuration.keyCode, "label" -> configuration.label))
      .toSeq
  }

}
