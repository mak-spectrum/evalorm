package io.evalorm.cfg.model


case class EvaluationFormRecord(keyCode: String, sections: Seq[EvaluationSectionData])

case class EvaluationSectionData(keyCode: String, fields: Seq[EvaluationFieldData])

case class EvaluationFieldData(keyCode: String, value: String)