package io.evalorm.cfg.model

case class EvaluationFormConfiguration(keyCode: String, label: String, sections: Seq[EvaluationSectionConfiguration])

case class EvaluationSectionConfiguration(keyCode: String, label: String, fields: Seq[EvaluationFieldConfiguration])

case class EvaluationFieldConfiguration(keyCode: String, label: String, fieldType: FieldType, options: Seq[EvaluationFieldConfigurationOption] = Seq())

case class EvaluationFieldConfigurationOption(label: String, value: String)