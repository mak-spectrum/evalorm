package io.evalorm.cfg.model

import enumeratum.EnumEntry.UpperSnakecase
import enumeratum.{CirceEnum, Enum, EnumEntry}

sealed abstract class FieldType extends EnumEntry with UpperSnakecase

object FieldType extends Enum[FieldType] with CirceEnum[FieldType] {

  val values = findValues

  case object Feedback extends FieldType
  case object DropDown extends FieldType
  case object Boolean extends FieldType

}
