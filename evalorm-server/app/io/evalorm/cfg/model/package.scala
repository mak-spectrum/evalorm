package io.evalorm.cfg

import io.circe.syntax.EncoderOps
import io.circe._

import scala.collection.immutable.ListMap

package object model {

  implicit val evaluationFormConfigurationDecoder: Decoder[EvaluationFormConfiguration] = new Decoder[EvaluationFormConfiguration] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFormConfiguration] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        label <- c.downField("label").as[String]
        sections <- c.downField("sections").as[Seq[EvaluationSectionConfiguration]]
      } yield {
        EvaluationFormConfiguration(keyCode, label, sections)
      }
  }

  implicit val evaluationSectionConfigurationDecoder: Decoder[EvaluationSectionConfiguration] = new Decoder[EvaluationSectionConfiguration] {
    final def apply(c: HCursor): Decoder.Result[EvaluationSectionConfiguration] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        label <- c.downField("label").as[String]
        fields <- c.downField("fields").as[Seq[EvaluationFieldConfiguration]]
      } yield {
        EvaluationSectionConfiguration(keyCode, label, fields)
      }
  }

  implicit val evaluationFieldConfigurationDecoder: Decoder[EvaluationFieldConfiguration] = new Decoder[EvaluationFieldConfiguration] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldConfiguration] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        label <- c.downField("label").as[String]
        fieldType <- c.downField("fieldType").as[FieldType]
        options <- c.downField("options").as[Option[Seq[EvaluationFieldConfigurationOption]]]
      } yield {
        EvaluationFieldConfiguration(keyCode, label, fieldType, options.getOrElse(Seq()))
      }
  }

  implicit val evaluationFieldConfigurationOptionDecoder: Decoder[EvaluationFieldConfigurationOption] = new Decoder[EvaluationFieldConfigurationOption] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldConfigurationOption] =
      for {
        label <- c.downField("label").as[String]
        value <- c.downField("value").as[String]
      } yield {
        EvaluationFieldConfigurationOption(label, value)
      }
  }



  implicit val evaluationFormConfigurationEncoder: Encoder[EvaluationFormConfiguration] = new Encoder[EvaluationFormConfiguration] {
    override def apply(f: EvaluationFormConfiguration): Json = Json.obj(
      "keyCode" -> Json.fromString(f.keyCode),
      "label" -> Json.fromString(f.label),
      "sections" -> Json.fromValues(f.sections.map(evaluationSectionConfigurationEncoder(_)))
    )
  }

  implicit val evaluationSectionConfigurationEncoder: Encoder[EvaluationSectionConfiguration] = new Encoder[EvaluationSectionConfiguration] {
    override def apply(s: EvaluationSectionConfiguration): Json = Json.obj(
      "keyCode" -> Json.fromString(s.keyCode),
      "label" -> Json.fromString(s.label),
      "fields" -> Json.fromValues(s.fields.map(evaluationFieldConfigurationEncoder(_)))
    )
  }

  implicit val evaluationFieldConfigurationEncoder: Encoder[EvaluationFieldConfiguration] = new Encoder[EvaluationFieldConfiguration] {
    override def apply(f: EvaluationFieldConfiguration): Json = {
      var map = ListMap[String, Json](
        "keyCode" -> Json.fromString(f.keyCode),
        "label" -> Json.fromString(f.label),
        "fieldType" -> f.fieldType.asJson,
      )

      if (f.options.nonEmpty) {
        map += ("options" -> Json.fromValues(f.options.map(evaluationFieldConfigurationOptionEncoder(_))))
      }
      Json.fromJsonObject(JsonObject.fromMap(map))
    }
  }

  implicit val evaluationFieldConfigurationOptionEncoder: Encoder[EvaluationFieldConfigurationOption] = new Encoder[EvaluationFieldConfigurationOption] {
    override def apply(o: EvaluationFieldConfigurationOption): Json = Json.obj(
      "label" -> Json.fromString(o.label),
      "value" -> Json.fromString(o.value)
    )
  }



  implicit val evaluationFormRecordDecoder: Decoder[EvaluationFormRecord] = new Decoder[EvaluationFormRecord] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFormRecord] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        sections <- c.downField("sections").as[Seq[EvaluationSectionData]]
      } yield {
        EvaluationFormRecord(keyCode, sections)
      }
  }

  implicit val evaluationSectionDataDecoder: Decoder[EvaluationSectionData] = new Decoder[EvaluationSectionData] {
    final def apply(c: HCursor): Decoder.Result[EvaluationSectionData] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        fields <- c.downField("fields").as[Seq[EvaluationFieldData]]
      } yield {
        EvaluationSectionData(keyCode, fields)
      }
  }

  implicit val evaluationFieldDataDecoder: Decoder[EvaluationFieldData] = new Decoder[EvaluationFieldData] {
    final def apply(c: HCursor): Decoder.Result[EvaluationFieldData] =
      for {
        keyCode <- c.downField("keyCode").as[String]
        value <- c.downField("value").as[String]
      } yield {
        EvaluationFieldData(keyCode, value)
      }
  }


  implicit val evaluationFormRecordEncoder: Encoder[EvaluationFormRecord] = new Encoder[EvaluationFormRecord] {
    override def apply(f: EvaluationFormRecord): Json = Json.obj(
      "keyCode" -> Json.fromString(f.keyCode),
      "sections" -> Json.fromValues(f.sections.map(evaluationSectionDataEncoder(_)))
    )
  }

  implicit val evaluationSectionDataEncoder: Encoder[EvaluationSectionData] = new Encoder[EvaluationSectionData] {
    override def apply(s: EvaluationSectionData): Json = Json.obj(
      "keyCode" -> Json.fromString(s.keyCode),
      "fields" -> Json.fromValues(s.fields.map(evaluationFieldDataEncoder(_)))
    )
  }

  implicit val evaluationFieldDataEncoder: Encoder[EvaluationFieldData] = new Encoder[EvaluationFieldData] {
    override def apply(f: EvaluationFieldData): Json = Json.obj(
      "keyCode" -> Json.fromString(f.keyCode),
      "value" -> Json.fromString(f.value),
    )
  }

}
