package io.evalorm.common.configuration

import com.typesafe.config.{Config, ConfigFactory}

object SystemConfiguration {

  private lazy val systemConfig: Config = ConfigFactory.load()

  def getDataStoreConfig: Config = systemConfig.getConfig("data-store")

}
