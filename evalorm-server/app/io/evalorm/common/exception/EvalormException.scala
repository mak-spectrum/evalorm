package io.evalorm.common.exception


class EvalormException(message: String = "", cause: Throwable = None.orNull) extends RuntimeException(message, cause)