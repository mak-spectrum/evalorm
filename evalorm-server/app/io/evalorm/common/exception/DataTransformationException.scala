package io.evalorm.common.exception

class DataTransformationException(message: String = "", cause: Throwable = None.orNull)
  extends EvalormException(message, cause)