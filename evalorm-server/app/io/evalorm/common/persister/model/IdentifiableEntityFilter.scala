package io.evalorm.common.persister.model

trait IdentifiableEntityFilter {

  def accept(obj: IdentifiableEntity): Boolean

}
