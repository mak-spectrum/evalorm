package io.evalorm.common.persister.model

trait IdentifiableEntity {

  def getUuid: Option[String]

  def setUuid(uuid: String): Unit

  def getAlias: String

}
