package io.evalorm.common.persister

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}


package object model {

  implicit val identifiableEntityKeyDecoder: Decoder[IdentifiableEntityKey] = deriveDecoder[IdentifiableEntityKey]

  implicit val identifiableEntityKeyEncoder: Encoder[IdentifiableEntityKey] = deriveEncoder[IdentifiableEntityKey]

}
