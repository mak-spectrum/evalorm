package io.evalorm.common.persister.model

class IdentifiableEntityKey(val uuid: String, val alias: Option[String] = None) {

  def canEqual(a: Any): Boolean = a.isInstanceOf[IdentifiableEntityKey]

  override def equals(that: Any): Boolean =
    that match {
      case that: IdentifiableEntityKey => that.canEqual(this) && this.uuid == that.uuid
      case _ => false
    }

  override def hashCode: Int = {
    val prime = 31
    prime * uuid.hashCode
  }

}