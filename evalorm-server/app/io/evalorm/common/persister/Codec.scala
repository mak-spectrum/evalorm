package io.evalorm.common.persister

trait Codec[Type] {

  def decode[RawType](rawForm: RawType)(implicit codecType: CodecType): Type

  def encode[RawType](obj: Type)(implicit codecType: CodecType): RawType

}
