package io.evalorm.common.persister.mongodb

import com.mongodb.DBObject
import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.commons.MongoDBObject
import io.evalorm.common.persister.model.{IdentifiableEntity, IdentifiableEntityFilter, IdentifiableEntityKey}
import io.evalorm.common.persister.{Codec, CodecType, IdentifiableEntityPersister}
import org.bson.types.ObjectId

trait MongoDBGenericPersister[T <: IdentifiableEntity] extends IdentifiableEntityPersister[T] with MongoDBSupport {

  implicit val codecType: CodecType = CodecType.MongoDBCodec

  lazy val collection: MongoCollection  = {
    if (!database.collectionExists(collectionName)) {
      val options: DBObject = MongoDBObject()
      database.createCollection(collectionName, options)
    }
    database(collectionName)
  }

  def collectionName: String

  def fetchList(filter: Option[IdentifiableEntityFilter]): Seq[IdentifiableEntityKey] = {
    collection.find().map(dBObject =>
      new IdentifiableEntityKey(
        dBObject.get(MongoDBConstants.UuidColumnName).asInstanceOf[ObjectId].toString,
        Option(dBObject.get(MongoDBConstants.AliasColumnName).asInstanceOf[String]))).toSeq
  }

  def fetchObject(uuid: String): Option[T] =
    collection.findOneByID(new ObjectId(uuid)).map(dbObject => codec.decode(dbObject))

  private def moldIdentifiableKeyDBObject(uuid: String): DBObject = {
    this.moldIdentifiableKeyDBObject(new ObjectId(uuid))
  }

  private def moldIdentifiableKeyDBObject(objectId: ObjectId): DBObject = {
    MongoDBObject(MongoDBConstants.UuidColumnName -> objectId)
  }


  def persistObject(uuid: Option[String], obj: T): IdentifiableEntityKey = {
    val objectId = uuid.map(uuidValue => new ObjectId(uuidValue)).getOrElse(new ObjectId())
    obj.setUuid(objectId.toHexString)

    val dBObject: DBObject = codec.encode[DBObject](obj)
    dBObject.put(MongoDBConstants.AliasColumnName, obj.getAlias)

    uuid match {
      case Some(_) =>
        collection.findAndModify(this.moldIdentifiableKeyDBObject(objectId), dBObject)
      case None =>
        collection.save(dBObject)
    }

    new IdentifiableEntityKey(objectId.toHexString)
  }

  def deleteObject(uuid: String): Boolean = {
    collection.findAndRemove(this.moldIdentifiableKeyDBObject(uuid)).isDefined
  }

  def codec: Codec[T]
}