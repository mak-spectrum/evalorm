package io.evalorm.common.persister.mongodb

object MongoDBConstants {

  val UuidColumnName = "_id"
  val AliasColumnName = "alias"

}
