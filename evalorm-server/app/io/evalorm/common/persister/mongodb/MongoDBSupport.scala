package io.evalorm.common.persister.mongodb

import com.mongodb.casbah.{MongoClient, MongoDB}

trait MongoDBSupport {

  lazy val client: MongoClient = MongoClient(config.host, config.port)

  lazy val database: MongoDB = client.getDB(config.database)

  def config: MongoDBDatastoreConfig

}