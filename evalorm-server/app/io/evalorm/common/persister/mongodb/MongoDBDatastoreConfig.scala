package io.evalorm.common.persister.mongodb

import com.typesafe.config.Config

case class DefaultMongoDBDatastoreConfig(dataStoreConfig: Config) extends MongoDBDatastoreConfig {

  import net.ceedubs.ficus.Ficus._

  private val mongodb = dataStoreConfig.as[Config]("mongodb")

  override def host: String = mongodb.getAs[String]("host").getOrElse("localhost")
  override def port: Int = mongodb.getAs[Int]("port").getOrElse(27017)
  override def database: String = mongodb.getAs[String]("database").getOrElse("evalorm")
}

trait MongoDBDatastoreConfig {

  def host: String

  def port: Int

  def database: String

}
