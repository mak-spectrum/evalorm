package io.evalorm.common.persister.exception

import io.evalorm.common.exception.EvalormException


class PersisterException(message: String = "", cause: Throwable = None.orNull) extends EvalormException(message, cause)
