package io.evalorm.common.persister

import enumeratum.{CirceEnum, Enum, EnumEntry}

sealed abstract class CodecType extends EnumEntry

object CodecType extends Enum[CodecType] with CirceEnum[CodecType] {

  val values = findValues

  case object MongoDBCodec extends CodecType

}

