package io.evalorm.common.persister

import com.typesafe.config.Config
import io.evalorm.common.persister.exception.PersisterException
import io.evalorm.common.persister.inmemory.InMemoryGenericPersister
import io.evalorm.common.persister.model.{IdentifiableEntity, IdentifiableEntityFilter, IdentifiableEntityKey}
import io.evalorm.common.persister.mongodb.{DefaultMongoDBDatastoreConfig, MongoDBGenericPersister}


trait IdentifiableEntityPersister[T <: IdentifiableEntity] {

  def fetchList(filter: Option[IdentifiableEntityFilter]): Seq[IdentifiableEntityKey]

  def fetchObject(uuid: String): Option[T]

  def persistObject(uuid: Option[String], obj: T): IdentifiableEntityKey

  def deleteObject(uuid: String): Boolean

}

object IdentifiableEntityPersister {

  val InMemoryDatastoreType = "inmemory"
  val MongoDBDatastoreType = "mongodb"

  def createPersister[T <: IdentifiableEntity](dataStoreConfiguration: Config, entityName: String)
      (implicit _codec: Codec[T]): IdentifiableEntityPersister[T] = {

    import net.ceedubs.ficus.Ficus._

    dataStoreConfiguration.as[Option[String]]("type").getOrElse(InMemoryDatastoreType) match {
      case InMemoryDatastoreType =>
        new Object with InMemoryGenericPersister[T]

      case MongoDBDatastoreType =>
        new Object with MongoDBGenericPersister[T] {
          override def codec: Codec[T] = _codec
          override def config = DefaultMongoDBDatastoreConfig(dataStoreConfiguration)
          override def collectionName: String = entityName
        }

      case _ => throw new PersisterException("Undefined database type.")
    }
  }

}