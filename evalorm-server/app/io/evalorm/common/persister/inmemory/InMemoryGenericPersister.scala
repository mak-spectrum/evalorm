package io.evalorm.common.persister.inmemory

import java.util.UUID

import io.evalorm.common.persister.IdentifiableEntityPersister
import io.evalorm.common.persister.model.{IdentifiableEntity, IdentifiableEntityFilter, IdentifiableEntityKey}

import scala.collection.mutable

trait InMemoryGenericPersister[T <: IdentifiableEntity] extends IdentifiableEntityPersister[T] {

  val storage: mutable.Map[IdentifiableEntityKey, T] =
      new mutable.HashMap[IdentifiableEntityKey, T]()

  def fetchList(filter: Option[IdentifiableEntityFilter]): Seq[IdentifiableEntityKey] =
    storage.toList map {
      case (key, _) => key
    }

  def fetchObject(uuid: String): Option[T] =
    storage.get(new IdentifiableEntityKey(uuid))

  def persistObject(uuid: Option[String], obj: T): IdentifiableEntityKey = {
    val uuidValue: String = uuid.getOrElse(UUID.randomUUID().toString)

    obj.setUuid(uuidValue)
    storage.put(new IdentifiableEntityKey(uuidValue, Some(obj.getAlias)), obj)
    new IdentifiableEntityKey(uuidValue)
  }

  def deleteObject(uuid: String): Boolean =
    storage.remove(new IdentifiableEntityKey(uuid)) match {
      case Some(_) => true
      case None => false
    }

}
