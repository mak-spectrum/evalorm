package io.evalorm.common.validation

import scala.collection.mutable
import io.evalorm.common.validation.MessageSevere.MessageSevere

import scala.collection.mutable.ListBuffer

class ValidationContext(var messages: mutable.ListBuffer[ValidationMessage] = new ListBuffer()) {
  def hasError: Boolean = messages.exists(m => m.isError)

  def addInfo(message: String, source: Option[String] = None): Unit =
    messages += ValidationMessage(message, source, MessageSevere.Info)

  def addWarning(message: String, source: Option[String] = None): Unit =
    messages += ValidationMessage(message, source, MessageSevere.Warning)

  def addError(message: String, source: Option[String] = None): Unit =
    messages += ValidationMessage(message, source, MessageSevere.Error)

  def consume(validationContext: ValidationContext): Unit =
    messages ++= validationContext.messages

  def getMessages: Seq[ValidationMessage] = messages.clone()

  def isEmpty: Boolean = messages.isEmpty
}

case class ValidationMessage(content: String, source: Option[String] = None, severe: MessageSevere = MessageSevere.Error) {
  def isError: Boolean = this.severe.equals(MessageSevere.Error)
  def isWarning: Boolean = this.severe.equals(MessageSevere.Warning)
  def isInfo: Boolean = this.severe.equals(MessageSevere.Info)
}

object MessageSevere extends Enumeration {
  type MessageSevere = Value
  val Error, Warning, Info = Value
}