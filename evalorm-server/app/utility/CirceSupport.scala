package utility

import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import cats.syntax.either._
import cats.syntax.show._
import akka.stream.scaladsl.{Flow, Sink}
import akka.stream.stage.{GraphStageLogic, GraphStageWithMaterializedValue, InHandler, OutHandler}
import akka.util.ByteString
import io.circe._
import play.api.http._
import play.api.libs.streams.Accumulator
import play.api.Logger
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.control.NonFatal

trait CirceSupport extends Status {
  self: AbstractController with HeaderNames =>

  private val defaultPrinter = Printer.noSpaces
  private implicit val executionContext: ExecutionContext = ExecutionContext.global

  def parse: PlayBodyParsers

  protected def onCirceError(e: Error): Result = {
    Results.BadRequest(e.show)
  }

  implicit val contentTypeOf_Json: ContentTypeOf[Json] = {
    ContentTypeOf(Some(ContentTypes.JSON))
  }

  implicit def writableOf_Json(implicit codec: Codec, printer: Printer = defaultPrinter): Writeable[Json] = {
    Writeable(a => codec.encode(a.pretty(printer)))
  }

  object circe {

    val logger = Logger(classOf[CirceSupport])

    def json[T: Decoder]: BodyParser[T] = json.validate(decodeJson[T])

    def json: BodyParser[Json] = json(parse.DefaultMaxTextLength)

    def json(maxLength: Int): BodyParser[Json] = parse.when(
      _.contentType.exists(m => m.equalsIgnoreCase("text/json") || m.equalsIgnoreCase("application/json")),
      tolerantJson(maxLength),
      createBadResult("Expecting text/json or application/json body", UNSUPPORTED_MEDIA_TYPE)
    )

    def tolerantJson[T: Decoder]: BodyParser[T] = tolerantJson.validate(decodeJson[T])

    def tolerantJson: BodyParser[Json] = tolerantJson(parse.DefaultMaxTextLength)

    def tolerantJson(maxLength: Int): BodyParser[Json] = {
      tolerantBodyParser[Json]("json", maxLength, "Invalid Json") { (request, bytes) =>
        val bodyString = new String(bytes.toArray[Byte], detectCharset(request))
        parser.parse(bodyString).leftMap(onCirceError)
      }
    }

    private def detectCharset(request: RequestHeader) = {
      val CharsetPattern = "(?i)\\bcharset=\\s*\"?([^\\s;\"]*)".r
      request.headers.get("Content-Type") match {
        case Some(CharsetPattern(c)) => c
        case _ => "UTF-8"
      }
    }

    private def decodeJson[T: Decoder](json: Json) = {
      implicitly[Decoder[T]].decodeJson(json).leftMap { ex =>
        logger.debug(s"Cannot decode json $json", ex)
        onCirceError(ex)
      }
    }

    private def createBadResult(msg: String, statusCode: Int = BAD_REQUEST): RequestHeader => Future[Result] = { request =>
      LazyHttpErrorHandler.onClientError(request, statusCode, msg)
    }

    private def tolerantBodyParser[A](name: String, maxLength: Int, errorMessage: String)(parser: (RequestHeader, ByteString) => Either[Result, A]): BodyParser[A] = {
      BodyParser(name + ", maxLength=" + maxLength) { request =>

        def parseBody(bytes: ByteString): Future[Either[Result, A]] = {
          try {
            Future.successful(parser(request, bytes))
          } catch {
            case NonFatal(e) =>
              logger.debug(errorMessage, e)
              createBadResult(errorMessage + ": " + e.getMessage)(request).map(Left(_))
          }
        }

        Accumulator.strict[ByteString, Either[Result, A]](
          // If the body was strict
          {
            case Some(bytes) if bytes.size <= maxLength =>
              parseBody(bytes)
            case None =>
              parseBody(ByteString.empty)
            case _ =>
              createBadResult("Request Entity Too Large", REQUEST_ENTITY_TOO_LARGE)(request).map(Left.apply)
          },
          // Otherwise, use an enforce max length accumulator on a folding sink
          enforceMaxLength(
            request,
            maxLength,
            Accumulator(Sink.fold[ByteString, ByteString](ByteString.empty)(
              (state, bs) => state ++ bs)
            ).mapFuture(parseBody)
          ).toSink
        )
      }
    }

    private def enforceMaxLength[A](
                                     request: RequestHeader,
                                     maxLength: Int,
                                     accumulator: Accumulator[ByteString, Either[Result, A]]):
    Accumulator[ByteString, Either[Result, A]] = {

      val takeUpToFlow = Flow.fromGraph(new TakeCirceBodyUpTo(maxLength.toLong))
      Accumulator(takeUpToFlow.toMat(accumulator.toSink) { (statusFuture, resultFuture) =>
        statusFuture.flatMap {
          case MaxSizeExceeded(_) =>
            val badResult = createBadResult("Request Entity Too Large", REQUEST_ENTITY_TOO_LARGE)(request)
            badResult.map(Left(_))

          case MaxSizeNotExceeded =>
            resultFuture
        }
      })
    }
  }

  // Graph class to read from Stream up to maxLength
  // Borrowed from play.api.mvc.BodyParsers
  private class TakeCirceBodyUpTo(maxLength: Long)
    extends GraphStageWithMaterializedValue[FlowShape[ByteString, ByteString], Future[MaxSizeStatus]] {

    private val in = Inlet[ByteString]("TakeCirceBodyUpTo.in")
    private val out = Outlet[ByteString]("TakeCirceBodyUpTo.out")

    override def shape: FlowShape[ByteString, ByteString] = FlowShape.of(in, out)

    override def createLogicAndMaterializedValue(inheritedAttributes: Attributes):
      (GraphStageLogic, Future[MaxSizeStatus]) = {

      val status = Promise[MaxSizeStatus]()
      var pushedBytes: Long = 0

      val logic = new GraphStageLogic(shape) {
        setHandler(out, new OutHandler {
          override def onPull(): Unit = {
            pull(in)
          }
          override def onDownstreamFinish(): Unit = {
            status.success(MaxSizeNotExceeded)
            completeStage()
          }
        })
        setHandler(in, new InHandler {
          override def onPush(): Unit = {
            val chunk = grab(in)
            pushedBytes += chunk.size
            if (pushedBytes > maxLength) {
              status.success(MaxSizeExceeded(maxLength))
              // Make sure we fail the stream, this will ensure downstream body parsers don't try to parse it
              failStage(new MaxLengthLimitAttained)
            } else {
              push(out, chunk)
            }
          }
          override def onUpstreamFinish(): Unit = {
            status.success(MaxSizeNotExceeded)
            completeStage()
          }
          override def onUpstreamFailure(ex: Throwable): Unit = {
            status.failure(ex)
            failStage(ex)
          }
        })
      }

      (logic, status.future)
    }
  }

  private class MaxLengthLimitAttained extends RuntimeException(null, null, false, false)
}