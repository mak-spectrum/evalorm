package utility

import play.api.http.MimeTypes
import play.api.libs.json.{JsBoolean, JsValue, Json}
import play.api.mvc.{BaseController, Request, Result}


trait BaseControllerConfiguration {
  self: BaseController =>

  private def origin[T](request: Request[T]): String =
    request.headers.get(ORIGIN).getOrElse("*")

  def errorObj(errors: List[String], fatal: Boolean = false): JsValue = Json.obj(
    "error" -> Json.obj(
      "messages" -> Json.toJson(errors),
      "fatal" -> JsBoolean(fatal)
    )
  )

  def jsonResult[T](result: Result)(implicit request: Request[T]): Result = {
    result.as(MimeTypes.JSON).withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> origin(request))
  }

}
