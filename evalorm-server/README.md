# Evalorm.io back-end

### General ###

* Experiment project for evaluation forms - REST API part
* Version 0.0.1

### How do I get set up? ###

* This is server side that is based on sbt + play-framework 2.6
* You need installing Scala 2.12.3 and Sbt 0.13.16
* You need install, configure and launch MongoDB (see instructions below)
* To run the project run `/>sbt run` within the root folder of evalorm-server project
* Find your application on [localhost:9000](http://localhost:9000)
* Tests in IDE are not always compliant with real scala code, because of Play compilation plugins. Run `>sbt test`, to actualize compiled binaries.

#### MongoDB Configuration on Windows ####

Download MongoDB Community Server by [link](http://www.mongodb.org/downloads)

MongoDB requires a directory to store data, logs and configuration. Let's assume you store data on `C:\` drive.

Create following folders and files:
* folder `C:\data\evalormdb\db`
* folder `C:\data\evalormdb\log`
* file `C:\data\evalormdb\mongod.cfg`

Put following configuration into the created file `mongod.cfg`:
```
systemLog:
    destination: file
    path: c:\data\evalormdb\log\mongod.log
storage:
    dbPath: c:\data\evalormdb\db
```

Now it is possible to install MongoDB, as a service.

It is assumed that MongoDB is installed into:
```
"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"
```

Following command will install MongoDB, as a service. Open Command Prompt or PowerShell ***with Administrator permissions*** and type:
```
"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" --config "C:\data\evalormdb\mongod.cfg" --install
```

If no error is shown, the service is installed correctly.

Now launch it through command line:
```
net start MongoDB
```

### Contribution guidelines ###

* Write tests
* Do code reviews

### Who do I talk to? ###

* mak.spectrum@gmail.com