package io.evalorm.form.model

import io.circe.parser.decode
import io.circe.syntax.EncoderOps
import io.evalorm.cfg.model.FieldType
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}


class EvaluationFormDataJsonSpec extends FlatSpec with Matchers with GivenWhenThen {

  val FormObject = EvaluationFormDataRecord(
    Some("_uuid_"),
    "testFormCode",
    "Test_Form",
    "Ivan",
    "Peter",
    "2017-Dec-1",
    Seq(
      EvaluationSectionData("testSection1Code", "Section_Label_1", Seq(
        EvaluationFieldData("field11Code", "Field_Label_11", FieldType.DropDown, StringFieldValue("field11Value")),
        EvaluationFieldData("field12Code", "Field_Label_12", FieldType.Boolean, BooleanFieldValue(true))
      )),
      EvaluationSectionData("testSection2Code", "Section_Label_2", Seq(
        EvaluationFieldData("field21Code", "Field_Label_21", FieldType.Feedback, FeedbackFieldValue("Good", 21)),
        EvaluationFieldData("field22Code", "Field_Label_22", FieldType.DropDown, StringFieldValue("field22Value"))
      ))
    )
  )

  val JsonObject: String =
    """
      |{
      |  "uuid": "_uuid_",
      |  "keyCode" : "testFormCode",
      |  "label" : "Test_Form",
      |  "candidateName" : "Ivan",
      |  "interviewedBy" : "Peter",
      |  "interviewDate" : "2017-Dec-1",
      |  "sections" : [
      |    {
      |      "keyCode" : "testSection1Code",
      |      "label" : "Section_Label_1",
      |      "fields" : [
      |        {
      |          "keyCode" : "field11Code",
      |          "label" : "Field_Label_11",
      |          "fieldType": "DROP_DOWN",
      |          "value" : "field11Value"
      |        },
      |        {
      |          "keyCode" : "field12Code",
      |          "label" : "Field_Label_12",
      |          "fieldType": "BOOLEAN",
      |          "value" : true
      |        }
      |      ]
      |    },
      |    {
      |      "keyCode" : "testSection2Code",
      |      "label" : "Section_Label_2",
      |      "fields" : [
      |        {
      |          "keyCode" : "field21Code",
      |          "label" : "Field_Label_21",
      |          "fieldType": "FEEDBACK",
      |          "value" : {
      |            "feedback": "Good",
      |            "score": 21
      |          }
      |        },
      |        {
      |          "keyCode" : "field22Code",
      |          "label" : "Field_Label_22",
      |          "fieldType": "DROP_DOWN",
      |          "value" : "field22Value"
      |        }
      |      ]
      |    }
      |  ]
      |}
    """.stripMargin.replaceAll(" ", "").replaceAll(System.getProperty("line.separator"), "")

  
  "EvaluationFormData" should "be encoded into JSON" in {
    Given("Simple object")
    val testObject = FormObject

    When("encode it")
    val jsonString: String = testObject.asJson.noSpaces

    Then("The result should match the given object")
    jsonString shouldBe JsonObject
  }

  it should "be decoded from JSON" in {
    Given("JSON string")
    val jsonString = JsonObject

    When("Parsing it")
    val result = decode[EvaluationFormDataRecord](jsonString) match {
      case Right(res) => res
      case Left(error) => error
    }

    Then("The result should match the given object")
    result shouldBe FormObject
  }

  it should "decode feedback EvaluationFieldData from JSON" in {
    Given("JSON string and feedback field data")
    val originalFieldValue = EvaluationFieldData("code", "Label", FieldType.Feedback, FeedbackFieldValue("Good_feedback", 21))
    val jsonString =
      """
        | {
        |   "keyCode" : "code",
        |   "label" : "Label",
        |   "fieldType": "FEEDBACK",
        |   "value" : {
        |     "feedback": "Good_feedback",
        |     "score": 21
        |   }
        | }
      """.stripMargin

    When("Parsing it")
    val result = decode[EvaluationFieldData](jsonString) match {
      case Right(res) => res
      case Left(error) => error
    }

    Then("The result should match the given object")
    result shouldBe originalFieldValue
  }

  it should "decode drop down EvaluationFieldData from JSON" in {
    Given("JSON string and drop down field data")
    val originalFieldValue = EvaluationFieldData("code", "Label", FieldType.DropDown, StringFieldValue("value A"))
    val jsonString =
      """
        | {
        |   "keyCode" : "code",
        |   "label" : "Label",
        |   "fieldType": "DROP_DOWN",
        |   "value" : "value A"
        | }
      """.stripMargin

    When("Parsing it")
    val result = decode[EvaluationFieldData](jsonString) match {
      case Right(res) => res
      case Left(error) => error
    }

    Then("The result should match the given object")
    result shouldBe originalFieldValue
  }

  it should "decode boolean EvaluationFieldData from JSON" in {
    Given("JSON string and boolean field data")
    val originalFieldValue = EvaluationFieldData("code", "Label", FieldType.Boolean, BooleanFieldValue(true))
    val jsonString =
      """
        | {
        |   "keyCode" : "code",
        |   "label" : "Label",
        |   "fieldType": "BOOLEAN",
        |   "value" : true
        | }
      """.stripMargin

    When("Parsing it")
    val result = decode[EvaluationFieldData](jsonString) match {
      case Right(res) => res
      case Left(error) => error
    }

    Then("The result should match the given object")
    result shouldBe originalFieldValue
  }

}