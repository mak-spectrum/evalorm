package io.evalorm.form.service

import com.github.fakemongo.Fongo
import com.mongodb.casbah.MongoDB
import io.evalorm.cfg.model.FieldType
import io.evalorm.common.persister.Codec
import io.evalorm.common.persister.model.IdentifiableEntityKey
import io.evalorm.common.persister.mongodb.{MongoDBDatastoreConfig, MongoDBGenericPersister}
import io.evalorm.form.model.{BooleanFieldValue, EvaluationFieldData, EvaluationFormDataRecord, EvaluationSectionData, FeedbackFieldValue, StringFieldValue, evaluationFormDataRecordCodec}
import org.scalatest.{BeforeAndAfter, FlatSpec, GivenWhenThen, Matchers}

class EvaluationFormDataRecordServiceSpec extends FlatSpec with Matchers with GivenWhenThen with BeforeAndAfter {

  val fongo = new Fongo("fake mongo server")

  val db: MongoDB = new MongoDB(fongo.getDB("testdb"))

  val testCollectionName = "test collection"

  val service: EvaluationFormDataRecordService = new EvaluationFormDataRecordServiceImpl {
    override lazy val persister = new MongoDBGenericPersister[EvaluationFormDataRecord] {
      override def collectionName: String = testCollectionName

      override def codec: Codec[EvaluationFormDataRecord] = evaluationFormDataRecordCodec

      override lazy val database: MongoDB = db

      override def config: MongoDBDatastoreConfig = ???
    }
  }

  val FormObject = EvaluationFormDataRecord(
    Some("_uuid_"),
    "testFormCode",
    "Test_Form",
    "Ivan",
    "Peter",
    "2017-Dec-1",
    Seq(
      EvaluationSectionData("testSection1Code", "Section_Label_1", Seq(
        EvaluationFieldData("field11Code", "Field_Label_11", FieldType.DropDown, StringFieldValue("field11Value")),
        EvaluationFieldData("field12Code", "Field_Label_12", FieldType.Boolean, BooleanFieldValue(true))
      )),
      EvaluationSectionData("testSection2Code", "Section_Label_2", Seq(
        EvaluationFieldData("field21Code", "Field_Label_21", FieldType.Feedback, FeedbackFieldValue("Good", 21)),
        EvaluationFieldData("field22Code", "Field_Label_22", FieldType.DropDown, StringFieldValue("field22Value"))
      ))
    )
  )

  after {
    val mongoCollection = db.getCollection(testCollectionName)
    val mongoCursor = mongoCollection.find()
    while (mongoCursor.hasNext) {
      val mongoObj = mongoCursor.next()
      mongoCollection.remove(mongoObj)
    }
  }

  "EvaluationFormDataRecordService" should "persist a new EvaluationFormDataRecord" in {
    Given("Sample object")
    val testObject = FormObject

    When("persisted")
    val idKey: IdentifiableEntityKey = service.persistObject(None, testObject)

    Then("The result should be a generated uuid")
    idKey.uuid should not be empty
  }

  it should "fetch by uuid the previously stored EvaluationFormDataRecord" in {
    Given("Sample object, saved by the persister")
    val sampleObject = FormObject
    val idKey: IdentifiableEntityKey = service.persistObject(None, sampleObject)

    When("Form is fetched by uuid")
    val form: Option[EvaluationFormDataRecord] = service.fetchObject(idKey.uuid)

    Then("The result should be previously stored object")
    form shouldBe an[Some[IdentifiableEntityKey]]
    form.get shouldBe sampleObject.copy(uuid = Some(idKey.uuid))
  }

  it should "fetch list of previously stored EvaluationFormDataRecord" in {
    Given("Three sample object, saved by the persister")
    val sampleObject = FormObject
    val firstLabel = "First Form"
    val secondLabel = "Second Form"
    val thirdLabel = "Third Form"
    service.persistObject(None, sampleObject.copy(label = firstLabel))
    service.persistObject(None, sampleObject.copy(label = secondLabel))
    service.persistObject(None, sampleObject.copy(label = thirdLabel))

    When("List is fetched")
    val list: Seq[IdentifiableEntityKey] = service.fetchList(None)

    Then("The result should be list of three keys and object should contain alias")
    list.size shouldBe 3
    list.find(k => k.alias.isDefined && k.alias.get == firstLabel) shouldBe an[Some[IdentifiableEntityKey]]
    list.find(k => k.alias.isDefined && k.alias.get == secondLabel) shouldBe an[Some[IdentifiableEntityKey]]
    list.find(k => k.alias.isDefined && k.alias.get == thirdLabel) shouldBe an[Some[IdentifiableEntityKey]]
  }

  it should "update previously stored EvaluationFormDataRecord" in {
    Given("Sample object, saved by the persister and its modified version")
    val sampleObject = FormObject
    val idKey: IdentifiableEntityKey = service.persistObject(None, sampleObject)
    val updatedObject = FormObject.copy(label = "Update Label", interviewedBy = "Joseph", interviewDate = "2017-Dec-1")

    When("the object is updated")
    service.persistObject(Some(idKey.uuid), updatedObject)

    Then("The reloaded result should be previously updated object")
    val form: Option[EvaluationFormDataRecord] = service.fetchObject(idKey.uuid)
    form shouldBe an[Some[IdentifiableEntityKey]]
    form.get shouldBe updatedObject.copy(uuid = Some(idKey.uuid))
  }

  it should "delete the previously stored EvaluationFormDataRecord" in {
    Given("Sample object, saved by the persister")
    val sampleObject = FormObject
    val idKey: IdentifiableEntityKey = service.persistObject(None, sampleObject)

    When("the object is deleted")
    val isDeleted: Boolean = service.deleteObject(idKey.uuid)

    Then("The deletion result should be true and the list of all objects should be empty")
    val list: Seq[IdentifiableEntityKey] = service.fetchList(None)

    isDeleted shouldBe true
    list shouldBe empty
  }

}