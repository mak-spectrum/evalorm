package io.evalorm.cfg.service

import io.evalorm.cfg.model.{EvaluationFieldConfiguration, EvaluationFormConfiguration, EvaluationSectionConfiguration, FieldType}
import io.evalorm.common.validation.{ValidationMessage, ValidationContext}
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}


class EvaluationFormConfigurationValidatorSpec extends FlatSpec with Matchers with GivenWhenThen {

  "EvaluationFormConfigurationValidator" should "check, if form code is empty" in {
    Given("form object without keyCode")
    val testForm = EvaluationFormConfiguration(null, "testFormLabel",
      Seq(EvaluationSectionConfiguration("testSectionCode", "testSectionLabel", Seq())))

    When("validation is done")
    val validationResult: ValidationContext = EvaluationFormConfigurationValidator.validateForm(testForm, "testFile")

    Then("validation result should contain a message")
    validationResult.getMessages.head shouldBe ValidationMessage("File [testFile]. Form keyCode is missed.")
  }

  it should "check that validation file name and form keyCode match" in {
    Given("form object with keyCode and another validation file name")
    val testForm = EvaluationFormConfiguration("testFormCode", "testFormLabel2",
      Seq(EvaluationSectionConfiguration("testSectionCode", "testSectionLabel", Seq())))

    When("validation is done")
    val validationResult: ValidationContext = EvaluationFormConfigurationValidator.validateForm(testForm, "anotherFile")

    Then("validation result should contain a message")
    validationResult.getMessages.head shouldBe
      ValidationMessage("File name [anotherFile] and form keyCode [testFormCode] don't match.")
  }

  it should "check, if form contains sections" in {
    Given("form object without sections")
    val testForm = EvaluationFormConfiguration("testFormCode", "testFormLabel", Seq())

    When("validation is done")
    val validationResult: ValidationContext =
      EvaluationFormConfigurationValidator.validateForm(testForm, "testFormCode")

    Then("validation result should contain a message")
    validationResult.getMessages.head shouldBe ValidationMessage("Form [testFormCode]. Form sections are missed.")
  }

  it should "check, if section keyCode is not unique" in {
    Given("form with several duplicated sections")
    val uniqueForm = EvaluationFormConfiguration("testFormCode", "testFormLabel",
      Seq(
        EvaluationSectionConfiguration("alphaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("alphaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("betaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("gammaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("deltaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("epsilonPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("zettaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("zettaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("etaPrimoCode", "alphaPrimoLabel", Seq()),
        EvaluationSectionConfiguration("thetaPrimoCode", "alphaPrimoLabel", Seq()),
      )
    )

    When("validation is done")
    val uniqueResult: ValidationContext = EvaluationFormConfigurationValidator.validateForm(uniqueForm, "testFormCode")

    Then("validation result should contain two messages")
    uniqueResult.getMessages(0) shouldBe
      ValidationMessage("Form [testFormCode]. Section keyCode [alphaPrimoCode] is not unique.")
    uniqueResult.getMessages(1) shouldBe
      ValidationMessage("Form [testFormCode]. Section keyCode [zettaPrimoCode] is not unique.")
  }

  it should "check, that section code is not empty" in {
    Given("form with section object without keyCode")
    val testForm = EvaluationFormConfiguration("testFormCode", "testFormLabel",
      Seq(EvaluationSectionConfiguration(null, "testSectionLabel", Seq())))

    When("validation is done")
    val validationResult: ValidationContext =
      EvaluationFormConfigurationValidator.validateForm(testForm, "testFormCode")

    Then("validation result should contain a message")
    validationResult.getMessages.head shouldBe ValidationMessage("Form [testFormCode]. Section keyCode is missed.")
  }

  it should "check, if field keyCode is not unique" in {
    Given("form with several duplicated fields")
    val uniqueFieldSection =
      EvaluationFormConfiguration("testFormCode", "testFormLabel",
        Seq(
          EvaluationSectionConfiguration("alphaSectionCode", "alphaSectionLabel",
            Seq(
              EvaluationFieldConfiguration("alphaPrimoCode",   "alphaPrimoLabel",   FieldType.Feedback),
              EvaluationFieldConfiguration("alphaPrimoCode",   "alphaPrimoLabel",   FieldType.Feedback),
              EvaluationFieldConfiguration("betaPrimoCode",    "betaPrimoLabel",    FieldType.Feedback),
              EvaluationFieldConfiguration("gammaPrimoCode",   "gammaPrimoLabel",   FieldType.Feedback),
              EvaluationFieldConfiguration("deltaPrimoCode",   "deltaPrimoLabel",   FieldType.Feedback),
              EvaluationFieldConfiguration("epsilonPrimoCode", "epsilonPrimoLabel", FieldType.Feedback),
              EvaluationFieldConfiguration("zetaPrimoCode",    "zetaPrimoLabel",    FieldType.Feedback),
              EvaluationFieldConfiguration("zetaPrimoCode",    "zetaPrimoLabel",    FieldType.Feedback),
              EvaluationFieldConfiguration("etaPrimoCode",     "etaPrimoLabel",     FieldType.Feedback),
              EvaluationFieldConfiguration("thetaPrimoCode",   "thetaPrimoLabel",   FieldType.Feedback),
            )
          )
        )
      )

    When("validation is done")
    val uniqueResult: ValidationContext =
      EvaluationFormConfigurationValidator.validateForm(uniqueFieldSection, "testFormCode")

    Then("validation result should contain two messages")
    uniqueResult.getMessages(0) shouldBe ValidationMessage(
      "Form [testFormCode]. Section [alphaSectionCode]. Field keyCode [alphaPrimoCode] is not unique.")
    uniqueResult.getMessages(1) shouldBe ValidationMessage(
      "Form [testFormCode]. Section [alphaSectionCode]. Field keyCode [zetaPrimoCode] is not unique.")
  }

  it should "check, if section contains no fields" in {
    Given("form with section object without fields")
    val testField =
      EvaluationFormConfiguration("testFormCode", "testFormLabel",
        Seq(
          EvaluationSectionConfiguration("alphaSectionCode", "alphaSectionLabel", Seq())
        )
      )

    When("validation is done")
    val validationResult: ValidationContext =
      EvaluationFormConfigurationValidator.validateForm(testField, "testFormCode")

    Then("validation result should contain a message")
    validationResult.getMessages.head shouldBe
      ValidationMessage("Form [testFormCode]. Section [alphaSectionCode] fields are missed.")
  }

  it should "check, that field code is not empty" in {
    Given("form with field object without keyCode")
    val testForm = EvaluationFormConfiguration("testFormCode", "testFormLabel",
      Seq(
        EvaluationSectionConfiguration("testSection", "testSectionLabel",
          Seq(
            EvaluationFieldConfiguration(null, "testFieldLabel", FieldType.Feedback)
          )
        )
      )
    )

    When("validation is done")
    val validationResult: ValidationContext =
      EvaluationFormConfigurationValidator.validateForm(testForm, "testFormCode")

    Then("validation result should contain a message")
    validationResult.getMessages.head shouldBe
      ValidationMessage("Form [testFormCode]. Section [testSection]. Field keyCode is missed.")
  }

  it should "mark validation, as passed, when form is correct" in {
    Given("correct form object")
    val testForm =
      EvaluationFormConfiguration("validationForm", "testFormLabel",
        Seq(
          EvaluationSectionConfiguration("alphaSectionCode", "alphaSectionLabel",
            Seq(
              EvaluationFieldConfiguration("alphaPrimoCode", "alphaPrimoLabel", FieldType.Feedback),
              EvaluationFieldConfiguration("alphaSecundusCode", "alphaSecundusLabel", FieldType.Feedback),
            )
          ),
          EvaluationSectionConfiguration("betaSectionCode", "betaSectionLabel",
            Seq(
              EvaluationFieldConfiguration("betaPrimoCode", "betaPrimoLabel", FieldType.Feedback),
              EvaluationFieldConfiguration("betaSecundusCode", "betaSecundusLabel", FieldType.Feedback),
            )
          )
        )
      )

    When("validation is done")
    val validationResult: ValidationContext =
      EvaluationFormConfigurationValidator.validateForm(testForm, "validationForm")

    Then("validation passed, form is correct.")
    validationResult.isEmpty shouldBe true
  }

}