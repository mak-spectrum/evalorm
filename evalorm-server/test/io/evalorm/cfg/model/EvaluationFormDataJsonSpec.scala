package io.evalorm.cfg.model

import io.circe.parser.decode
import io.circe.syntax._
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}


class EvaluationFormDataJsonSpec extends FlatSpec with Matchers with GivenWhenThen {

  val FormObject = EvaluationFormRecord("testFormCode", Seq(
    EvaluationSectionData("testSection1Code", Seq(
      EvaluationFieldData("field11Code", "field11Value"),
      EvaluationFieldData("field12Code", "field12Value")
    )),
    EvaluationSectionData("testSection2Code", Seq(
      EvaluationFieldData("field21Code", "field21Value"),
      EvaluationFieldData("field22Code", "field22Value")
    ))
  ))

  val JsonObject: String =
    """
      |{
      |  "keyCode" : "testFormCode",
      |  "sections" : [
      |    {
      |      "keyCode" : "testSection1Code",
      |      "fields" : [
      |        {
      |          "keyCode" : "field11Code",
      |          "value" : "field11Value"
      |        },
      |        {
      |          "keyCode" : "field12Code",
      |          "value" : "field12Value"
      |        }
      |      ]
      |    },
      |    {
      |      "keyCode" : "testSection2Code",
      |      "fields" : [
      |        {
      |          "keyCode" : "field21Code",
      |          "value" : "field21Value"
      |        },
      |        {
      |          "keyCode" : "field22Code",
      |          "value" : "field22Value"
      |        }
      |      ]
      |    }
      |  ]
      |}
    """.stripMargin.replaceAll(" ", "").replaceAll(System.getProperty("line.separator"), "")

  
  "EvaluationFormData" should "be encoded into JSON" in {
    Given("Simple object")
    val testObject = FormObject

    When("encode it")
    val jsonString: String = testObject.asJson.noSpaces

    Then("The result should match the given object")
    jsonString shouldBe JsonObject
  }

  it should "be decoded from JSON" in {
    Given("JSON string")
    val jsonString = JsonObject

    When("Parsing it")
    val result = decode[EvaluationFormRecord](jsonString) match {
      case Right(res) => res
      case Left(error) => error
    }

    Then("The result should match the given object")
    result shouldBe FormObject
  }

}