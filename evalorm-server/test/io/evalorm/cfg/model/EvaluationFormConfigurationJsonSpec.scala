package io.evalorm.cfg.model

import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import io.circe.syntax._
import io.circe.parser.decode


class EvaluationFormConfigurationJsonSpec extends FlatSpec with Matchers with GivenWhenThen {

  val FormObject = EvaluationFormConfiguration("testFormCode", "testFormLabel", Seq(
    EvaluationSectionConfiguration("testSection1Code", "testSection1Label", Seq(
      EvaluationFieldConfiguration("field11Code", "field11Label", FieldType.Feedback),
      EvaluationFieldConfiguration("field12Code", "field12Label", FieldType.DropDown, Seq(
        EvaluationFieldConfigurationOption("optionLabel1", "optionValue1"),
        EvaluationFieldConfigurationOption("optionLabel2", "optionValue2"),
        EvaluationFieldConfigurationOption("optionLabel3", "optionValue3"),
        EvaluationFieldConfigurationOption("optionLabel4", "optionValue4")
      ))
    )),
    EvaluationSectionConfiguration("testSection2Code", "testSection2Label", Seq(
      EvaluationFieldConfiguration("field21Code", "field21Label", FieldType.Feedback),
      EvaluationFieldConfiguration("field22Code", "field22Label", FieldType.Boolean)
    ))
  ))

  val JsonObject: String =
    """
      |{
      |  "keyCode" : "testFormCode",
      |  "label" : "testFormLabel",
      |  "sections" : [
      |    {
      |      "keyCode" : "testSection1Code",
      |      "label" : "testSection1Label",
      |      "fields" : [
      |        {
      |          "keyCode" : "field11Code",
      |          "label" : "field11Label",
      |          "fieldType" : "FEEDBACK"
      |        },
      |        {
      |          "keyCode" : "field12Code",
      |          "label" : "field12Label",
      |          "fieldType" : "DROP_DOWN",
      |          "options": [
      |            {
      |              "label" : "optionLabel1",
      |              "value" : "optionValue1"
      |            },
      |            {
      |              "label" : "optionLabel2",
      |              "value" : "optionValue2"
      |            },
      |            {
      |              "label" : "optionLabel3",
      |              "value" : "optionValue3"
      |            },
      |            {
      |              "label" : "optionLabel4",
      |              "value" : "optionValue4"
      |            }
      |          ]
      |        }
      |      ]
      |    },
      |    {
      |      "keyCode" : "testSection2Code",
      |      "label" : "testSection2Label",
      |      "fields" : [
      |        {
      |          "keyCode" : "field21Code",
      |          "label" : "field21Label",
      |          "fieldType" : "FEEDBACK"
      |        },
      |        {
      |          "keyCode" : "field22Code",
      |          "label" : "field22Label",
      |          "fieldType" : "BOOLEAN"
      |        }
      |      ]
      |    }
      |  ]
      |}
    """.stripMargin.replaceAll(" ", "").replaceAll(System.getProperty("line.separator"), "")


  "EvaluationFormConfiguration" should "be encoded into JSON" in {
    Given("Simple object")
    val testObject = FormObject

    When("encode it")
    val jsonString: String = testObject.asJson.noSpaces

    Then("The result should match the given object")
    jsonString shouldBe JsonObject
  }

  it should "be decoded from JSON" in {
    Given("JSON string")
    val jsonString = JsonObject

    When("Parsing it")
    val result = decode[EvaluationFormConfiguration](jsonString) match {
      case Right(res) => res
      case Left(error) => error
    }

    Then("The result should match the given object")
    result shouldBe FormObject
  }

}