package controllers

import akka.stream.Materializer
import io.circe.parser.decode
import io.circe.syntax.EncoderOps
import io.evalorm.cfg.model.{EvaluationFormConfiguration, FieldType}
import io.evalorm.common.persister.model.IdentifiableEntityKey
import io.evalorm.form.model.{BooleanFieldValue, EvaluationFieldData, EvaluationFormDataRecord, EvaluationSectionData, FeedbackFieldValue, StringFieldValue}
import org.scalatest.GivenWhenThen
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.http.{ContentTypes, HeaderNames}
import play.api.test.Helpers._
import play.api.test.{FakeHeaders, FakeRequest, Injecting, NoMaterializer}

class EvaluationFormControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting with GivenWhenThen {

  implicit val disabledMaterializer: Materializer = NoMaterializer

  val fakeHeaders = FakeHeaders(
    Seq(
      HeaderNames.ACCEPT -> "application/json",
      HeaderNames.HOST -> "localhost",
      HeaderNames.CONTENT_TYPE -> "application/json"
    )
  )

  val FormObject = EvaluationFormDataRecord(
    Some("_uuid_"),
    "testFormCode",
    "Test_Form",
    "Ivan",
    "Peter",
    "2017-Dec-1",
    Seq(
      EvaluationSectionData("testSection1Code", "Section_Label_1", Seq(
        EvaluationFieldData("field11Code", "Field_Label_11", FieldType.DropDown, StringFieldValue("field11Value")),
        EvaluationFieldData("field12Code", "Field_Label_12", FieldType.Boolean, BooleanFieldValue(true))
      )),
      EvaluationSectionData("testSection2Code", "Section_Label_2", Seq(
        EvaluationFieldData("field21Code", "Field_Label_21", FieldType.Feedback, FeedbackFieldValue("Good", 21)),
        EvaluationFieldData("field22Code", "Field_Label_22", FieldType.DropDown, StringFieldValue("field22Value"))
      ))
    )
  )

  "EvaluationFormController" should {

    "provide form configuration by keyCode" in {
      Given("Request to form configuration read")
      val readConfigRequest = FakeRequest(GET, "/api/v1/configuration/forms/evaluationFormControllerTestForm")

      When("Request is executed")
      val readRequestResult = route(app, readConfigRequest).get

      Then("Result must be successful")
      status(readRequestResult) mustBe OK
      contentType(readRequestResult) mustBe Some(ContentTypes.JSON)

      And("Content readable")
      val form = decode[EvaluationFormConfiguration](contentAsString(readRequestResult)).right.get
      form.keyCode must equal ("evaluationFormControllerTestForm")
    }

    "provide not-found for nonexistent form configuration keyCode" in {
      Given("Request to form configuration read")
      val readConfigRequest = FakeRequest(GET, "/api/v1/configuration/forms/nonexistentTestForm")

      When("Request is executed")
      val readRequestResult = route(app, readConfigRequest).get

      Then("Result must be NOT_FOUND")
      status(readRequestResult) mustBe NOT_FOUND
    }

    "get list of available forms configurations" in {
      val configsList = Seq(
        Map("keyCode" -> "evaluationFormControllerTestForm", "label" -> "My Test Form"),
        Map("keyCode" -> "secondTestForm", "label" -> "My Test Form Two"),
        Map("keyCode" -> "thirdTestForm", "label" -> "My Test Form Three")
      )
      Given("Request to read configurations list")
      val getListRequest = FakeRequest(GET, "/api/v1/configuration/forms")

      When("Request is executed")
      val readRequestResult = route(app, getListRequest).get

      Then("Result must be successful")
      status(readRequestResult) mustBe OK
      contentType(readRequestResult) mustBe Some(ContentTypes.JSON)

      And("Content readable")
      val form = decode[List[Map[String, String]]](contentAsString(readRequestResult)).right.get
      form mustBe configsList
    }



    "create a new EvaluationFormRecord instance on POST" in {
      Given("Request to form record creation")
      val createRequest = FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders, FormObject.asJson.noSpaces)

      When("Request is executed")
      val createRequestResult = route(app, createRequest).get

      Then("Result must be successful")
      status(createRequestResult) mustBe OK
      contentType(createRequestResult) mustBe Some(ContentTypes.JSON)

      And("Result should contain uuid of the created object")
      val key = decode[IdentifiableEntityKey](contentAsString(createRequestResult)).right.get
      key.uuid must not be empty
    }

    "read the previously created EvaluationFormRecord instance on GET with uuid in the path" in {
      Given("Result of create request, created object uuid and read request")
      val createRequestResult = route(app,
        FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders, FormObject.asJson.noSpaces)
      ).get
      val key = decode[IdentifiableEntityKey](contentAsString(createRequestResult)).right.get
      val readRequest = FakeRequest(GET, s"/api/v1/form-data-records/${key.uuid}")

      When("Request is executed")
      val readRequestResult = route(app, readRequest).get

      Then("Result must be successful")
      status(readRequestResult) mustBe OK
      contentType(readRequestResult) mustBe Some(ContentTypes.JSON)

      And("Result should contain the originally created object with populated UUID")
      val _object = decode[EvaluationFormDataRecord](contentAsString(readRequestResult)).right.get
      _object mustBe FormObject.copy(uuid = _object.uuid)
    }

    "read list of the previously created EvaluationFormRecord instance on GET" in {
      Given("3 executed create requests, read list request")
      val firstLabel = "FIRST_IN_LIST"
      val secondLabel = "SECOND_IN_LIST"
      val thirdLabel = "THIRD_IN_LIST"
      route(app, FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders,
        FormObject.copy(label = firstLabel).asJson.noSpaces))
      route(app, FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders,
        FormObject.copy(label = secondLabel).asJson.noSpaces))
      route(app, FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders,
        FormObject.copy(label = thirdLabel).asJson.noSpaces))
      val readListRequest = FakeRequest(GET, s"/api/v1/form-data-records")

      When("Request is executed")
      val readRequestResult = route(app, readListRequest).get

      Then("Result must be successful")
      status(readRequestResult) mustBe OK
      contentType(readRequestResult) mustBe Some(ContentTypes.JSON)

      And("Result list should contain three originally created object")
      val list = decode[Seq[IdentifiableEntityKey]](contentAsString(readRequestResult)).right.get
      list.length mustBe 3
      list.find(r => r.alias.isDefined && r.alias.get == firstLabel) mustBe an[Some[IdentifiableEntityKey]]
      list.find(r => r.alias.isDefined && r.alias.get == secondLabel) mustBe an[Some[IdentifiableEntityKey]]
      list.find(r => r.alias.isDefined && r.alias.get == thirdLabel) mustBe an[Some[IdentifiableEntityKey]]
    }

    "update the previously created EvaluationFormRecord instance on PUT with uuid in the path" in {
      Given("Result of create request, created object uuid, updated form, PUT request and GET request to do checks")
      val createRequestResult = route(app,
        FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders, FormObject.asJson.noSpaces)
      ).get
      val updatedFormObject = FormObject.copy(keyCode = "UPDATED")
      val key = decode[IdentifiableEntityKey](contentAsString(createRequestResult)).right.get
      val putRequest = FakeRequest(PUT, s"/api/v1/form-data-records/${key.uuid}", fakeHeaders,
        updatedFormObject.asJson.noSpaces)
      val readRequest = FakeRequest(GET, s"/api/v1/form-data-records/${key.uuid}")

      When("Request is executed")
      val putRequestResult = route(app, putRequest).get

      Then("Result must be successful")
      status(putRequestResult) mustBe OK
      contentType(putRequestResult) mustBe Some(ContentTypes.JSON)

      And("Read request should return the updated object with populated UUID")
      val readRequestResult = route(app, readRequest).get
      val _object = decode[EvaluationFormDataRecord](contentAsString(readRequestResult)).right.get
      _object mustBe updatedFormObject.copy(uuid = _object.uuid)

    }

    "delete the previously created EvaluationFormRecord instance on DELETE with uuid in the path" in {
      Given("Result of create request, created object uuid and DELETE request")
      val createRequestResult = route(app,
        FakeRequest(POST, "/api/v1/form-data-records", fakeHeaders, FormObject.asJson.noSpaces)
      ).get
      val key = decode[IdentifiableEntityKey](contentAsString(createRequestResult)).right.get
      val deleteRequest = FakeRequest(DELETE, s"/api/v1/form-data-records/${key.uuid}")
      val readListRequest = FakeRequest(GET, s"/api/v1/form-data-records")

      When("Request is executed")
      val deleteRequestResult = route(app, deleteRequest).get

      Then("Result must be successful")
      status(deleteRequestResult) mustBe NO_CONTENT

      And("Read list request should return empty Seq")
      val list = decode[Seq[IdentifiableEntityKey]](contentAsString(route(app, readListRequest).get)).right.get
      list.length mustBe 0
    }

    "receive NotFound on an atempt to delete an unexisting object" in {
      Given("DELETE request")
      val deleteRequest = FakeRequest(DELETE, s"/api/v1/form-data-records/unexisting")

      When("Request is executed")
      val deleteRequestResult = route(app, deleteRequest).get

      Then("Result must be failed with not found")
      status(deleteRequestResult) mustBe NOT_FOUND
    }

  }

}
