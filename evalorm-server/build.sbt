import sbt.ExclusionRule

name := """evalorm-server"""
organization := "io.evalorm"

scalaVersion := "2.12.3"
version := "0.0.1-SNAPSHOT"


// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies += guice

// common
libraryDependencies ++= {
  val circeVersion = "0.8.0"

  Seq(
    "com.iheart" %% "ficus" % "1.4.3",
    "com.beachape" %% "enumeratum" % "1.5.12",
    "com.beachape" %% "enumeratum-circe" % "1.5.12",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"
      excludeAll ExclusionRule(organization = "io.circe")
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)
}

// mongo
libraryDependencies += "org.mongodb" %% "casbah" % "3.1.1"

// tests
libraryDependencies ++= {
  Seq(
    "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.1" % Test,
    "org.scalatest" %% "scalatest" % "3.0.0" % Test,
    "com.github.fakemongo" % "fongo" % "2.1.0" % Test
  )
}

routesGenerator := InjectedRoutesGenerator

// Adds additional packages into Twirl
// TwirlKeys.templateImports += "io.evalorm.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "io.evalorm.binders._"
