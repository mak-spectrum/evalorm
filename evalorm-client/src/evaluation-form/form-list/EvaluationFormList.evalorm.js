import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {Panel, Button, ListGroup, ListGroupItem} from 'react-bootstrap';

class EvaluationFormList extends Component {

  renderTable() {
    const renderLists = this.props.efRecordsList.map(form => this.renderRow(form));

    return (
        <ListGroup>
          <h4>Forms</h4>
          {renderLists}
        </ListGroup>
    );
  };

  renderRow(form) {

    return (
        <ListGroupItem key={form.uuid} onClick={() => this.props.onSelect(form.uuid)}>
          {form.alias}
        </ListGroupItem>
    );
  };

  render() {
    return (
        <div>
          <Panel>
            {this.renderTable()}
          </Panel>
          <Button className='ef-editor-save-button' bsStyle='primary' onClick={this.props.onCreate}>Create Form</Button>
        </div>
    );
  };
};

EvaluationFormList.propTypes = {
  efRecordsList: PropTypes.array.isRequired,
  onCreate: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired
};

export default EvaluationFormList;