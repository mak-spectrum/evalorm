import { EvaluationFormAsyncActions, EvaluationFormActions } from './actions.evalorm'
import EvaluationFormPages from './EvaluationFormPages.evalorm';

export default function evaluationFormManagerApp(state, action) {
  state = handleFormUpdateActions(state, action);
  state = handleEFConfigurationDataAsyncActions(state, action);
  state = handleEFDataRecordAsyncActions(state, action);
  state = handleBackButtonActions(state, action);
  return state;
}

function handleEFConfigurationDataAsyncActions(state, action) {
  switch (action.type) {
    case EvaluationFormAsyncActions.EF_CFG_FETCH_REQUEST_SENT:
      state.loading = true;
      return state;

    case EvaluationFormAsyncActions.EF_CFG_FETCH_REQUEST_SUCCESSFUL:
      const formWithNewCfg = {
        cfg: action.value,
        // we do create empty form template, when config is gotten
        data: prepareFormDataTemplate(action.value),
        cfgList: state.form.cfgList
      };
      return {
        ...state,
        loading: false,
        currentPage: EvaluationFormPages.EVALUATION_FORM_EDITOR,
        form: formWithNewCfg
      };

    case EvaluationFormAsyncActions.EF_CFG_FETCH_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    case EvaluationFormAsyncActions.EF_CFG_LIST_REQUEST_SENT:
      state.loading = true;
      return state;

    case EvaluationFormAsyncActions.EF_CFG_LIST_REQUEST_SUCCESSFUL:
      const formWithNewCfgList = {
        // other fields must be cleared, when config is loaded for the new form is creation
        cfgList: action.value
      };
      return {
        ...state,
        currentPage: EvaluationFormPages.EVALUATION_FORM_CREATOR,
        loading: false,
        form: formWithNewCfgList
      };

    case EvaluationFormAsyncActions.EF_CFG_LIST_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    default:
      return state;
  }
};

function handleEFDataRecordAsyncActions(state, action) {
  switch (action.type) {
    case EvaluationFormAsyncActions.EF_RECORD_SAVE_REQUEST_SENT:
      state.loading = true;
      return state;

    case EvaluationFormAsyncActions.EF_RECORD_SAVE_REQUEST_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        currentPage: EvaluationFormPages.EVALUATION_FORMS_LIST
      };

    case EvaluationFormAsyncActions.EF_RECORD_SAVE_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    case EvaluationFormAsyncActions.EF_RECORD_LIST_REQUEST_SENT:
      state.loading = true;
      return state;

    case EvaluationFormAsyncActions.EF_RECORD_LIST_REQUEST_SUCCESSFUL:
      return {
        ...state,
        currentPage: EvaluationFormPages.EVALUATION_FORMS_LIST,
        loading: false,
        efRecordsList: action.value
      };

    case EvaluationFormAsyncActions.EF_RECORD_LIST_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    case EvaluationFormAsyncActions.EF_RECORD_FETCH_REQUEST_SENT:
      return {
        ...state,
        loading: true
      };
    case EvaluationFormAsyncActions.EF_RECORD_FETCH_REQUEST_SUCCESSFUL:
      const loadedForm = {
        data: action.value
      };
      return {
        ...state,
        currentPage: EvaluationFormPages.EVALUATION_FORM_VIEWER,
        loading: true,
        form: loadedForm
      };

    case EvaluationFormAsyncActions.EF_RECORD_FETCH_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    case EvaluationFormAsyncActions.EF_RECORD_REMOVE_REQUEST_SENT:
      state.loading = true;
      return state;

    case EvaluationFormAsyncActions.EF_RECORD_REMOVE_REQUEST_SUCCESSFUL:
      const dForm = {
          data: {},
          cfg: null,
          cfgList: []
        };
      return {
        ...state,
        currentPage: EvaluationFormPages.EVALUATION_FORMS_LIST,
        loading: true,
        form: dForm
      };

    case EvaluationFormAsyncActions.EF_RECORD_REMOVE_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    default:
      return state;
  }
};

function handleFormUpdateActions(state, action) {
  switch (action.type) {
    case EvaluationFormActions.EF_DATA_FIELD_CHANGE:
      const formData = state.form.data;
      const fieldState = action.value;

      if (fieldState.section) {
        const section = formData.sections[fieldState.section];

        if (section.fields[fieldState.key].fieldType === 'BOOLEAN') {
          section.fields[fieldState.key].value = JSON.parse(fieldState.value);
        } else if (section.fields[fieldState.key].fieldType === 'DROP_DOWN') {
          section.fields[fieldState.key].value.selected = fieldState.value;
        } else {
          section.fields[fieldState.key].value = fieldState.value;
        }
      } else {
        formData[fieldState.key] = fieldState.value;
      }

      const formWithNewData = {
        cfg: state.form.cfg,
        data: formData,
        cfgList: state.form.cfgList
      };

      return {
        ...state,
        form: formWithNewData
      };


    default:
      return state;
  }
}

function prepareFormDataTemplate(cfg) {
  const formData = {
    keyCode: cfg.keyCode,
    label: cfg.label,
    sections: {}
  };

  cfg.sections.forEach(sectionCfg => {
    const sectionData = {
      keyCode: sectionCfg.keyCode,
      label: sectionCfg.label,
      fields: {}
    };
    formData.sections[sectionCfg.keyCode] = sectionData;
    sectionCfg.fields.forEach(fieldCfg => {
      let fieldData = {
        keyCode: fieldCfg.keyCode,
        label: fieldCfg.label,
        fieldType: fieldCfg.fieldType
      };
      if (fieldCfg.fieldType === 'DROP_DOWN') {
        fieldData = {
          keyCode: fieldCfg.keyCode,
          label: fieldCfg.label,
          fieldType: fieldCfg.fieldType,
          value: {
            selected: '',
            options: fieldCfg.options
          }
        };
      }

      if (fieldData.fieldType === 'FEEDBACK') {
        fieldData.value = {feedback: 'N/A', score: '-'};
      } else if (fieldData.fieldType === 'BOOLEAN') {
        fieldData.value = JSON.parse('false');
      }

      sectionData.fields[fieldCfg.keyCode] = fieldData;
    });
  });

  return formData;
}

function handleBackButtonActions(state, action) {
    switch (action.type) {
      case EvaluationFormActions.EF_BACK_TO_RECORDS_LIST:
        return {
          ...state,
          currentPage: EvaluationFormPages.EVALUATION_FORMS_LIST
        };

      case EvaluationFormActions.EF_BACK_TO_CREATOR_LIST:
        return {
        ...state,
          currentPage: EvaluationFormPages.EVALUATION_FORM_CREATOR
        };

      default:
        return state;
    }
}
