import HttpError from '../common/errors/HttpError.evalorm'

// EVALUATION FORM ACTIONS
// configuration fetch
const EF_CFG_FETCH_REQUEST_SENT = 'EVALUATION_FORM_CONFIGURATION_FETCH_REQUEST_SENT';
const EF_CFG_FETCH_REQUEST_SUCCESSFUL = 'EVALUATION_FORM_CONFIGURATION_FETCH_REQUEST_SUCCESSFUL';
const EF_CFG_FETCH_REQUEST_FAILED = 'EVALUATION_FORM_CONFIGURATION_FETCH_REQUEST_FAILED';

//loading forms list
const EF_RECORD_LIST_REQUEST_SENT = 'EF_RECORD_LIST_REQUEST_SENT';
const EF_RECORD_LIST_REQUEST_SUCCESSFUL = 'EF_RECORD_LIST_REQUEST_SUCCESSFUL';
const EF_RECORD_LIST_REQUEST_FAILED = 'EF_RECORD_LIST_REQUEST_FAILED';

// load available form configurations
const EF_CFG_LIST_REQUEST_SENT = 'EF_CFG_LIST_REQUEST_SENT';
const EF_CFG_LIST_REQUEST_SUCCESSFUL = 'EF_CFG_LIST_REQUEST_SUCCESSFUL';
const EF_CFG_LIST_REQUEST_FAILED = 'EF_CFG_LIST_REQUEST_FAILED';

// client-server data exchange
const EF_RECORD_SAVE_REQUEST_SENT = 'EVALUATION_FORM_DATA_RECORD_SAVE_REQUEST_SENT';
const EF_RECORD_SAVE_REQUEST_SUCCESSFUL = 'EVALUATION_FORM_DATA_RECORD_SAVE_REQUEST_SUCCESSFUL';
const EF_RECORD_SAVE_REQUEST_FAILED = 'EVALUATION_FORM_DATA_RECORD_SAVE_REQUEST_FAILED';

const EF_RECORD_FETCH_REQUEST_SENT = 'EVALUATION_FORM_DATA_RECORD_FETCH_REQUEST_SENT';
const EF_RECORD_FETCH_REQUEST_SUCCESSFUL = 'EVALUATION_FORM_DATA_RECORD_FETCH_REQUEST_SUCCESSFUL';
const EF_RECORD_FETCH_REQUEST_FAILED = 'EVALUATION_FORM_DATA_RECORD_FETCH_REQUEST_FAILED';

// data processing
const EF_DATA_FIELD_CHANGE = 'EVALUATION_FORM_DATA_FIELD_CHANGE';

const EF_RECORD_REMOVE_REQUEST_SENT = 'EVALUATION_FORM_RECORD_REMOVE_REQUEST_SENT';
const EF_RECORD_REMOVE_REQUEST_SUCCESSFUL = 'EVALUATION_FORM_RECORD_REMOVE_REQUEST_SUCCESSFUL';
const EF_RECORD_REMOVE_REQUEST_FAILED = 'EVALUATION_FORM_RECORD_REMOVE_REQUEST_FAILED';

//back to page actions
const EF_BACK_TO_RECORDS_LIST = 'EF_BACK_TO_RECORDS_LIST';
const EF_BACK_TO_CREATOR_LIST = 'EF_BACK_TO_CREATOR_LIST';


function defaultHeaders(contentLength) {
  const myHeaders = new Headers();

  myHeaders.append('Accept', 'application/json');
  myHeaders.append('Content-Type', 'application/json');
  myHeaders.append('Content-Length', contentLength.toString());

  return myHeaders;
}

// EVALUATION FORM ACTIONS
// configuration fetch
function fetchEFConfiguration(formKeyCode) {
  return (dispatch, getState) => {
    dispatch(emitEFConfigurationFetchRequestSent());

    fetch('/api/v1/configuration/forms/' + formKeyCode)
    .then(
        response => {
          if (!response.ok) {
            throw new HttpError(response.status, response.statusText);
          }
          return response.json();
        }
    )
    .then(
        json => dispatch(emitEFConfigurationFetchRequestSuccessful(json))
    )
    .catch(
        error => {
          console.error('An error occurred.', error);
          dispatch(emitEFConfigurationFetchRequestFailed(error));
        }
    );
  }
};

function emitEFConfigurationFetchRequestSent() {
  return {type: EF_CFG_FETCH_REQUEST_SENT};
};

function emitEFConfigurationFetchRequestSuccessful(form) {
  return {type: EF_CFG_FETCH_REQUEST_SUCCESSFUL, value: form};
};

function emitEFConfigurationFetchRequestFailed(error) {
  return {type: EF_CFG_FETCH_REQUEST_FAILED, value: error};
};

function fetchEFRecordsList() {
  return (dispatch, getState) => {
    dispatch(emitEFRecordsListSent());

    fetch('/api/v1/form-data-records')
    .then(
        response => {
          if (!response.ok) {
            throw new HttpError(response.status, response.statusText);
          }
          return response.json();
        }
    )
    .then(
        json => dispatch(emitEFRecordsListSuccessful(json))
    )
    .catch(
        error => {
          console.error('An error occurred.', error);
          dispatch(emitEFRecordsListFailed(error));
        }
    );
  }
}

function emitEFRecordsListSent() {
  return {type: EF_RECORD_LIST_REQUEST_SENT};
};

function emitEFRecordsListSuccessful(forms) {
  return {type: EF_RECORD_LIST_REQUEST_SUCCESSFUL, value: forms};
};

function emitEFRecordsListFailed(error) {
  return {type: EF_RECORD_LIST_REQUEST_FAILED, value: error};
};

function fetchEFRecord(uuid) {
  return (dispatch, getState) => {
    dispatch(emitEFRecordFetchRequestSent());

    fetch('/api/v1/form-data-records/' + uuid)
    .then(
        response => {
          if (!response.ok) {
            throw new HttpError(response.status, response.statusText);
          }
          return response.json();
        }
    )
    .then(
        parsedJsonObject => dispatch(emitEFRecordFetchRequestSuccessful(parsedJsonObject))
    )
    .catch(
        error => {
            console.error('An error occurred.', error);
            dispatch(emitEFRecordFetchRequestFailed(error));
        }
    );
  }
}

function emitEFRecordFetchRequestSent() {
  return {type: EF_RECORD_FETCH_REQUEST_SENT};
};

function emitEFRecordFetchRequestSuccessful(form) {
  return {type: EF_RECORD_FETCH_REQUEST_SUCCESSFUL, value: form};
};

function emitEFRecordFetchRequestFailed(error) {
  return {type: EF_RECORD_FETCH_REQUEST_FAILED, value: error};
};

function fetchEFConfigurationsList() {
  return (dispatch, getState) => {
    dispatch(emitEFConfigurationsListSent());

    fetch('/api/v1/configuration/forms')
    .then(
        response => {
          if (!response.ok) {
            throw new HttpError(response.status, response.statusText);
          }
          return response.json();
        }
    )
    .then(
        json => dispatch(emitEFConfigurationsListSuccessful(json))
    )
    .catch(
        error => {
          console.error('An error occurred.', error);
          dispatch(emitEFConfigurationsListFailed(error));
        }
    );
  }
}

function emitEFConfigurationsListSent() {
  return {type: EF_CFG_LIST_REQUEST_SENT};
};

function emitEFConfigurationsListSuccessful(forms) {
  return {type: EF_CFG_LIST_REQUEST_SUCCESSFUL, value: forms};
};

function emitEFConfigurationsListFailed(error) {
  return {type: EF_CFG_LIST_REQUEST_FAILED, value: error};
};

// client-server data exchange
function saveEvaluationForm() {
  return (dispatch, getState) => {
    dispatch(emitEFRecordSaveRequestSent());

    const formKeyCode = getState().form.cfg.keyCode
    const form = getState().form.data;
    const candidateNameValue = form.candidateName
    const formLabel = form.label;
    const interviewedByValue = form.interviewedBy
    const interviewDateValue = form.interviewDate

    const result = {
      keyCode: formKeyCode,
      candidateName: candidateNameValue,
      interviewedBy: interviewedByValue,
      interviewDate: interviewDateValue,
      label: formLabel,
      sections: []
    };

    for (let sectionName in form.sections) {
      const sectionData = form.sections[sectionName];
      const section = {
        keyCode: sectionName,
        label: sectionData.label,
        fields: []
      };
      for (let fieldName in sectionData.fields) {
        const fieldData = sectionData.fields[fieldName];
        let fieldValue = fieldData.value;
        if (fieldData.fieldType === 'DROP_DOWN') {
          fieldValue = {
            selected: fieldData.value.selected,
            options: fieldData.value.options
          };
        } else {
          fieldValue = fieldData.value;
        }
          section.fields.push({
            keyCode: fieldName,
            label: fieldData.label,
            fieldType: fieldData.fieldType,
            value: fieldValue
          });
      };
      result.sections.push(section);
    };

    const content = JSON.stringify(result);

    fetch('/api/v1/form-data-records',
        {
          method: 'post',
          headers: defaultHeaders(content.length),
          body: content
        }
    )
    .then(response => {
        dispatch(emitEFRecordSaveRequestSuccessful());
        dispatch(fetchEFRecordsList());
    })
    .catch(error => {
        console.error('An error occurred.', error);
        dispatch(emitEFRecordSaveRequestFailed(error));
    });
  }
};

function emitEFRecordSaveRequestSent() {
  return {type: EF_RECORD_SAVE_REQUEST_SENT};
};

function emitEFRecordSaveRequestSuccessful() {
  return {type: EF_RECORD_SAVE_REQUEST_SUCCESSFUL};
};

function emitEFRecordSaveRequestFailed(error) {
  return {type: EF_RECORD_SAVE_REQUEST_FAILED, value: error};
};

function emitEFBackToRecordsList() {
  return {type: EF_BACK_TO_RECORDS_LIST};
};

function emitEFBackToCreatorList() {
  return {type: EF_BACK_TO_CREATOR_LIST};
};

function removeEFRecord(uuid) {
  return (dispatch, getState) => {
    dispatch(emitEFRecordRemoveRequestSent());

    fetch('/api/v1/form-data-records/' + uuid,
    {
      method: 'DELETE'
    })
    .then(
        response => {
          if (response.status !== 204) {
            throw new HttpError(response.status, response.statusText);
          }
          return '';
        }
    )
    .then(
        () => {
          dispatch(emitEFRecordRemoveRequestSuccessful());
          dispatch(fetchEFRecordsList());
        }
    )
    .catch(
        error => {
            console.error('An error occurred.', error);
            dispatch(emitEFRecordRemoveRequestFailed(error));
        }
    );
  }
};

function emitEFRecordRemoveRequestSent() {
  return {type: EF_RECORD_REMOVE_REQUEST_SENT};
};

function emitEFRecordRemoveRequestSuccessful() {
  return {type: EF_RECORD_REMOVE_REQUEST_SUCCESSFUL};
};

function emitEFRecordRemoveRequestFailed(error) {
  return {type: EF_RECORD_REMOVE_REQUEST_FAILED, value: error};
};

// data processing
function emitEFDataFieldChange(fieldState) {
  return {type: EF_DATA_FIELD_CHANGE, value: fieldState};
};

export const EvaluationFormAsyncActions = {
  fetchEFConfiguration,
  EF_CFG_FETCH_REQUEST_SENT,
  EF_CFG_FETCH_REQUEST_SUCCESSFUL,
  EF_CFG_FETCH_REQUEST_FAILED,

  fetchEFRecordsList,
  EF_RECORD_LIST_REQUEST_SENT,
  EF_RECORD_LIST_REQUEST_SUCCESSFUL,
  EF_RECORD_LIST_REQUEST_FAILED,

  fetchEFConfigurationsList,
  EF_CFG_LIST_REQUEST_SENT,
  EF_CFG_LIST_REQUEST_SUCCESSFUL,
  EF_CFG_LIST_REQUEST_FAILED,

  saveEvaluationForm,
  EF_RECORD_SAVE_REQUEST_SENT,
  EF_RECORD_SAVE_REQUEST_SUCCESSFUL,
  EF_RECORD_SAVE_REQUEST_FAILED,

  fetchEFRecord,
  EF_RECORD_FETCH_REQUEST_SENT,
  EF_RECORD_FETCH_REQUEST_SUCCESSFUL,
  EF_RECORD_FETCH_REQUEST_FAILED,

  removeEFRecord,
  EF_RECORD_REMOVE_REQUEST_SENT,
  EF_RECORD_REMOVE_REQUEST_SUCCESSFUL,
  EF_RECORD_REMOVE_REQUEST_FAILED
};

export const EvaluationFormActions = {
  emitEFBackToRecordsList,
  emitEFBackToCreatorList,
  EF_BACK_TO_RECORDS_LIST,
  EF_BACK_TO_CREATOR_LIST,

  emitEFDataFieldChange,
  EF_DATA_FIELD_CHANGE
};
