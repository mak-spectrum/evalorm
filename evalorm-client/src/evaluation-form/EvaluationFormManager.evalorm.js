import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { EvaluationFormAsyncActions, EvaluationFormActions } from './actions.evalorm.js';

import HttpError from '../common/errors/HttpError.evalorm.js';

import { Grid, Row, Col, Panel, Label } from 'react-bootstrap';

import './EvaluationFormManager.evalorm.css';
import EvaluationFormEditor from './form-editor/EvaluationFormEditor.evalorm.js';
import EvaluationFormList from './form-list/EvaluationFormList.evalorm.js';
import EvaluationConfigurationsList from './form-editor/EvaluationFormCreator.evalorm.js';
import EvaluationFormPages from './EvaluationFormPages.evalorm';
import EvaluationFormViewer from "./form-editor/EvaluationFormViewer.evalorm";


class EvaluationFormManager extends Component {
//  TODO: this sample is needed to show, how dispatch can be called from a component; remove it later
//  componentDidMount() {
//    const { dispatch } = this.props
//    dispatch(fetchPostsIfNeeded(selectedSubreddit))
//  };

  render() {
    let header = 'Evaluation Form Manager';
    let rootView;
    if (this.props.globalError) {
      rootView = <h4><Label bsStyle='danger'> There was an error: {this.props.globalError.message} </Label></h4>;
    } else if (this.props.currentPage === EvaluationFormPages.EVALUATION_FORMS_LIST) {
      rootView = <EvaluationFormList efRecordsList={this.props.efRecordsList}
                                     onCreate={this.props.onEvaluationFormCreate}
                                     onSelect={this.props.onEvaluationFormFetch} />;

      header = <span><b>{'Submitted Forms'}</b></span>;
    } else if (this.props.currentPage === EvaluationFormPages.EVALUATION_FORM_CREATOR) {
      rootView = <EvaluationConfigurationsList
          efConfigurationsList={this.props.form.cfgList}
          onBack={this.props.onBackToRecordsList}
          onCfgSelect={this.props.onEvaluationFormCfgSelect} />;
      header = <span><b>{'Available forms'}</b></span>;
    } else if (this.props.currentPage === EvaluationFormPages.EVALUATION_FORM_EDITOR) {
      rootView = (
          <EvaluationFormEditor
              formCfg={this.props.form.cfg}
              formData={this.props.form.data}
              onDataFieldChange={this.props.onEFDataFieldChange}
              onSave={this.props.onEvaluationFormSave}
              onBack={this.props.onBackToCreatorList}
          />
      );
      header = <span>{'Editing '}<b>{'"' + this.props.form.cfg.label + '"'}</b></span>;
    } else if (this.props.currentPage === EvaluationFormPages.EVALUATION_FORM_VIEWER) {
      rootView = (
          <EvaluationFormViewer
              formData={this.props.form.data}
              onBack={this.props.onBackToRecordsList}
              onDelete={this.props.onEvaluationFormDelete}
          />
      );
    }

    return (
        <Grid fluid={true}>
          <Row>
            <Col xs={8} md={8} xsOffset={2} mdOffset={2}>
              <Panel className='evalorm-main-panel' header={header}>
                {rootView}
              </Panel>
            </Col>
          </Row>
        </Grid>
    );
  };
};

EvaluationFormManager.propTypes = {
  currentPage: PropTypes.string,
  globalError: PropTypes.oneOfType([PropTypes.instanceOf(HttpError)]),
  formCfg: PropTypes.shape({
    keyCode: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    sections: PropTypes.arrayOf(
        PropTypes.shape({
          keyCode: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired,
          fields: PropTypes.arrayOf(
              PropTypes.shape({
                keyCode: PropTypes.string.isRequired,
                label: PropTypes.string.isRequired,
                fieldType: PropTypes.string.isRequired
              })
          )
        })
    )
  }),
  formData: PropTypes.object,
  efRecordsList: PropTypes.array,
  efConfigurationsList: PropTypes.array,

  onEFDataFieldChange: PropTypes.func.isRequired,
  onEvaluationFormSave: PropTypes.func.isRequired,
  onEvaluationFormFetch: PropTypes.func.isRequired,
  onEvaluationFormDelete: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    currentPage: state.currentPage,
    globalError: state.globalError,
    efRecordsList: state.efRecordsList,
    form: state.form
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onEFDataFieldChange: (fieldState) => {
      dispatch(EvaluationFormActions.emitEFDataFieldChange(fieldState));
    },
    onEvaluationFormSave: () => {
      dispatch(EvaluationFormAsyncActions.saveEvaluationForm());
    },
    onEvaluationFormDelete: (uuid) => {
      dispatch(EvaluationFormAsyncActions.removeEFRecord(uuid));
    },
    onEvaluationFormCreate: () => {
      dispatch(EvaluationFormAsyncActions.fetchEFConfigurationsList());
    },
    onEvaluationFormCfgSelect: (configuration) => {
      dispatch(EvaluationFormAsyncActions.fetchEFConfiguration(configuration));
    },
    onEvaluationFormFetch: (uuid) => {
      dispatch(EvaluationFormAsyncActions.fetchEFRecord(uuid));
    },
    onBackToRecordsList: () => {
      dispatch(EvaluationFormActions.emitEFBackToRecordsList());
    },
    onBackToCreatorList: () => {
      dispatch(EvaluationFormActions.emitEFBackToCreatorList());
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EvaluationFormManager);
