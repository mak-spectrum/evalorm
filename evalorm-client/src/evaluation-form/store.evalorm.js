import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'

const loggerMiddleware = createLogger();

const initialState = {
    loading: false,
    globalError: null,
    efRecordsList: [],

    form: {
      data: {},
      cfg: null,
      cfgList: []
    }
};

export default function configureStore(rootReducer, preLoadedState) {
  return createStore(
    rootReducer,
    {
        ...initialState,
        ...preLoadedState,
    },
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
}
