import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {ListGroup, ListGroupItem, Button} from 'react-bootstrap';


class EvaluationFormConfigurationsList extends Component {

  renderTable() {
    const renderTableRows = this.props.efConfigurationsList.map(form => this.renderTableRow(form));

    return (
        <ListGroup>
          <h4>Configurations</h4>
          {renderTableRows}
        </ListGroup>
      );
  };

  renderTableRow(configuration) {
    return (
        <ListGroupItem key={configuration.keyCode} onClick={() => this.props.onCfgSelect(configuration.keyCode)}>
          {configuration.label}
        </ListGroupItem>
      );
  };

  render() {
    const renderedTable = this.renderTable();

    return (
        <div>
          {renderedTable}
          <Button className='ef-editor-back-button' bsStyle='primary' onClick={this.props.onBack}>Back</Button>
        </div>
      );
  };
};

EvaluationFormConfigurationsList.propTypes = {
  efConfigurationsList: PropTypes.array.isRequired,
  onCfgSelect: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired
};

export default EvaluationFormConfigurationsList;