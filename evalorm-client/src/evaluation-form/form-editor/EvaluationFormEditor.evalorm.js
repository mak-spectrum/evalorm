import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import DatePicker from 'react-bootstrap-date-picker';
import { ControlLabel, Button, FormControl, FormGroup, Panel, Radio } from 'react-bootstrap';

import ScoredFeedbackInput from '../../components/scored-feedback-input/ScoredFeedbackInput.evalorm';

import './EvaluationFormEditor.evalorm.css';


class EvaluationFormEditor extends Component {

  provideFeedbacksList(fieldCfg) {
    return ['He is young and interested in studying and growing.',
    'Smart and communicable person with good mathematical and technical background.',
    'He has experience working with very complex tracking systems and has good understanding, how it is built.']
        .map(itemText => new ScoredFeedbackInput.ItemWrapper(uuid.v4(), itemText, Math.ceil(Math.random() * 100)));
  };

  parseFieldKeys = (complexName) => {
    const FIELD_KEY_DELIMITER = ':';
    const delimiterPosition = complexName.indexOf(FIELD_KEY_DELIMITER);

    return {
      section: complexName.substring(0, delimiterPosition),
      key: complexName.substring(delimiterPosition + 1),
    }
  };

  onDynamicFieldChange = (evt) => {
    const parsedKeys = this.parseFieldKeys(evt.target.name);
    let fieldState = {
      section: parsedKeys.section,
      key: parsedKeys.key,
      value: evt.target.value
    };

    this.props.onDataFieldChange(fieldState);
  };

  onStaticFieldChange = (evt) => {
    let fieldState = {
      key: evt.target.name,
      value: evt.target.value
    };

    this.props.onDataFieldChange(fieldState);
  };

  onFeedbackFieldChange = (evt) => {
    const parsedKeys = this.parseFieldKeys(evt.name);
    const fieldState = {
      section: parsedKeys.section,
      key: parsedKeys.key,
      value: {
        feedback: evt.element.feedback,
        score: evt.element.score,
      }
    };

    this.props.onDataFieldChange(fieldState);
  };

  onInterviewDateChange = (date) => {
    let fieldState = {
      key: 'interviewDate',
      value: date
    };

    this.props.onDataFieldChange(fieldState);
  };

  renderField(complexKeyCode, fieldCfg, fieldData) {
    return (
        <FormGroup key={'form-group:' + complexKeyCode} controlId={'form-control:' + complexKeyCode}>
          {this.renderInputControl(complexKeyCode, fieldCfg, fieldData.value)}
        </FormGroup>
    );
  };

  renderInputControl(complexKeyCode, fieldCfg, fieldValue) {
    if (fieldCfg.fieldType === 'FEEDBACK') {

      let scoreValue;
      if (fieldValue) {
        if (fieldValue instanceof ScoredFeedbackInput.ItemWrapper) {
          scoreValue = fieldValue;
        } else if (fieldValue.feedback && fieldValue.score) {
          scoreValue = new ScoredFeedbackInput.ItemWrapper(uuid.v4(), fieldValue.feedback, fieldValue.score);
        }
      }

      return (
          <ScoredFeedbackInput
              label={fieldCfg.label}
              name={complexKeyCode}
              list={this.provideFeedbacksList(fieldCfg)}
              placeholder='type your feedback...'
              value={scoreValue}
              onChange={this.onFeedbackFieldChange}
              onSelect={this.onFeedbackFieldChange}
          />
      );
    } else if (fieldCfg.fieldType === 'BOOLEAN') {
      // TODO: Need to rework logic for not asked field
      return [
          (<ControlLabel key='label'>{fieldCfg.label}</ControlLabel>),
          (<div key='field'>
            <Radio name={complexKeyCode} onChange={this.onDynamicFieldChange} value={true} inline>
              Yes
            </Radio>
            <Radio name={complexKeyCode} onChange={this.onDynamicFieldChange} value={false} inline>
              No
            </Radio>
            <Radio name={complexKeyCode} onChange={this.onDynamicFieldChange} value={false} inline>
              Not Asked
            </Radio>
          </div>)
      ];
    } else if (fieldCfg.fieldType === 'DROP_DOWN') {
      const options = fieldCfg.options.map(option => this.renderOptions(option));
      return [
          (<ControlLabel key='label'>{fieldCfg.label}</ControlLabel>),
          (<FormControl
              key='field'
              name={complexKeyCode}
              componentClass='select'
              value={fieldValue.selected}
              onChange={this.onDynamicFieldChange}
            >
            <option value=''>--select value--</option>
            {options}
          </FormControl>)
      ];
    } else {
      return [
          (<ControlLabel key='label'>{fieldCfg.label}</ControlLabel>),
          (<FormControl.Static key='field'>
            ---
          </FormControl.Static>)
      ];
    }
  };

  renderOptions(option) {
    return (
      <option value={option.value} key={option.value}>{option.label}</option>
    );
  };

  renderSection(sectionCfg, sectionData) {
    const sectionKeyCode = sectionCfg.keyCode;
    const renderedFields = sectionCfg.fields.map(
        fieldCfg => this.renderField(sectionKeyCode + ':' + fieldCfg.keyCode, fieldCfg, sectionData.fields[fieldCfg.keyCode])
    );

    return (
        <Panel key={'section-panel:' + sectionKeyCode} header={sectionCfg.label}>
          {renderedFields}
        </Panel>
    );
  };

  render() {
    const renderedSections = this.props.formCfg.sections.map(
        sectionCfg => this.renderSection(sectionCfg, this.props.formData.sections[sectionCfg.keyCode])
    );

    return (
        <div>
          <ControlLabel>Candidate Name</ControlLabel>
          <FormControl
              name='candidateName'
              value={this.props.formData.candidateName}
              className='ef-static-input'
              type='text'
              placeholder='Candidate Name'
              onChange={this.onStaticFieldChange}
          />
          <ControlLabel>Interviewed By</ControlLabel>
          <FormControl
              name='interviewedBy'
              value={this.props.formData.interviewedBy}
              className='ef-static-input'
              type='text'
              placeholder='Interviewers Names'
              onChange={this.onStaticFieldChange}
          />
          <FormGroup>
            <ControlLabel>Interview Date</ControlLabel>
            <DatePicker
                name="interviewDate"
                value={this.props.formData.interviewDate}
                className='ef-static-input'
                dateFormat='YYYY-MM-DD'
                placeholder='Pick interview data'
                showClearButton={false}
                onChange={this.onInterviewDateChange}
            />
          </FormGroup>
          {renderedSections}
          <Button className='ef-editor-save-button' bsStyle='primary' onClick={this.props.onSave}>Save</Button>
          <Button className='ef-editor-back-button' bsStyle='primary' onClick={this.props.onBack}>Back</Button>
        </div>
    );
  };
};

EvaluationFormEditor.propTypes = {
  formCfg: PropTypes.shape({
    keyCode: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    sections: PropTypes.arrayOf(
        PropTypes.shape({
          keyCode: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired,
          fields: PropTypes.arrayOf(
              PropTypes.shape({
                keyCode: PropTypes.string.isRequired,
                label: PropTypes.string.isRequired,
                fieldType: PropTypes.string.isRequired
              })
          )
        })
    )
  }),
  formData: PropTypes.object,

  onDataFieldChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired
};

export default EvaluationFormEditor;
