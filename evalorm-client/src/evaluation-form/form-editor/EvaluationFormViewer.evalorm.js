import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ControlLabel, Panel, Button} from 'react-bootstrap';
import './EvaluationFormViewer.evalorm.css'

class EvaluationFormViewer extends Component {
  _formatDate(interviewDate) {
    const timeFormatDelimiter = 'T';
    const timeStartIndex = interviewDate.indexOf(timeFormatDelimiter);
    return interviewDate.substring(0, timeStartIndex);
  };
  render() {
    const renderedSections = this.props.formData.sections.map((sectionData) => this.renderSection(sectionData));
    return (
        <div className='ef-form-viewer'>
          <div className='ef-form-field'>
            <ControlLabel className='ef-form-caption'>Candidate Name</ControlLabel>
            <div>{this.props.formData.candidateName}</div>
          </div>

          <div className='ef-form-field'>
            <ControlLabel className='ef-form-caption'>Interviewed By</ControlLabel>
            <div>{this.props.formData.interviewedBy}</div>
          </div>

          <div className='ef-form-field'>
            <ControlLabel className='ef-form-caption'>Interview Date</ControlLabel>
            <div>{this._formatDate(this.props.formData.interviewDate.toString())}</div>
          </div>

          <div>{renderedSections}</div>
          <Button className='ef-editor-back-button' bsStyle='primary' onClick={this.props.onBack}>Back</Button>
          <Button
            className='ef-editor-delete-button'
            bsStyle='danger'
            onClick={() => this.props.onDelete(this.props.formData.uuid)}>
            Delete
          </Button>
        </div>
    );

  };

  renderSection(section) {
    const insertField = section.fields.map(fieldData => this.renderField(fieldData));
    return (
        <Panel key={section.keyCode} header={section.label}>
          <div>{insertField}</div>
        </Panel>
    );
  }

  renderField(field) {
    return (
        <div className='ef-form-field' key={field.keyCode}>
          <ControlLabel className='ef-form-caption'>{field.label}</ControlLabel>
          <div>{this.renderFieldValue(field.fieldType, field.value)}</div>
        </div>
    )
  }

  renderFieldValue(fieldType, fieldValue) {
    if (fieldType === 'FEEDBACK') {
      return (
          <div>
            <div>{fieldValue.feedback}</div>
            <div className='ef-score-value'>{fieldValue.score}/100</div>
          </div>
      );
    } else if (fieldType === 'DROP_DOWN') {
      return (
          <div>{fieldValue.selected}</div>
      );
    } else {
      return (
          <div>{fieldValue.toString()}</div>
      );
    }
  }
};

EvaluationFormViewer.propTypes = {
  formData: PropTypes.object,
  onBack: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default EvaluationFormViewer;
