import React from 'react';
import ReactDOM from 'react-dom';

import EvaluationFormManager from '../evaluation-form/EvaluationFormManager.evalorm';


// FIXME: non-functional now, need to see, how do testing with redux
const initialState = {
    loading: false,
    editorEnabled: false,
    globalError: null,
    formCfg: null
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EvaluationFormManager />, div);
});
