export default function HttpError(statusCode, message) {
  this.name = 'HttpError';
  this.statusCode = (statusCode || '-');
  this.message = (message || '');
}
HttpError.prototype = new Error();