import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

import './ScoredFeedbackInput.evalorm.css';


const FEEDBACK_NOT_ASKED = 'N/A'
const SCORE_NOT_ASKED = '-'

const ARROW_UP_KEY = 'ArrowUp'
const ARROW_DOWN_KEY = 'ArrowDown'
const ESCAPE_KEY = 'Escape'
const ENTER_KEY = 'Enter'
const TAB_KEY = 'Tab'


class ScoredFeedbackInput extends Component {

  constructor(props) {
    super(props);

    this.state = {
      list: [],

      feedbackFocused: false,
      scoreFocused: false,

      feedback: FEEDBACK_NOT_ASKED,
      score: SCORE_NOT_ASKED,

      showListPopup: false,

      selectedItem: null
    }
  }

  componentWillMount() {
    const newState = {};

    if (this.props.list) {
      newState.list = this.props.list;
    }

    if (this.props.value) {
      newState.feedback = this.props.value.feedback
      newState.score = this.props.value.score;
      newState.selectedItem = this.props.value;
    }

    this.setState(newState);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      const newState = {};

      if (nextProps.list) {
        newState.list = nextProps.list;
      }

      if (nextProps.value) {
        newState.feedback = nextProps.value.feedback;
        newState.score = nextProps.value.score;
        newState.selectedItem = nextProps.value;
      }

      this.setState(newState);
    }
  };

  handleFeedbackFocus = (evt) => {
    this.setState({feedbackFocused: true});

    // plain DOM manipulation
    if (evt.target.value === FEEDBACK_NOT_ASKED) {
      evt.target.select();
    }
  };

  handleFeedbackBlur = () => {
    this.setState({feedbackFocused: false});
  };

  handleScoreFocus = (evt) => {
    this.setState({scoreFocused: true});

    // plain DOM manipulation
    if (evt.target.value === SCORE_NOT_ASKED) {
      evt.target.select();
    }
  };

  handleScoreBlur = (evt) => {
    const newState = {scoreFocused: false};

    const score = evt.target.value;
    if (score.trim().length === 0) {
      newState.score = SCORE_NOT_ASKED;
    }
    this.setState(newState)
  };

  handleFeedbackChange = (evt) => {
    this.setState({
      feedback: evt.target.value,
      showListPopup: true,
      selectedItem: null
    });

    if (this.props.onChange) {
      this.props.onChange(
        {
          name: this.props.name,
          element: ScoredItemWrapper.wrapValue(evt.target.value, this.state.score)
        }
      );
    }
  };

  handleScoreChange = (evt) => {
    let score = evt.target.value;
    const scoreNumeric = parseFloat(score);

    if (!isNaN(scoreNumeric)) {
      if (scoreNumeric < 0) {
        score = 0;
      } else if (scoreNumeric > 100) {
        score = 100;
      } else {
        score = scoreNumeric.toString();
      }
    }

    if (!isNaN(scoreNumeric)
        || score === SCORE_NOT_ASKED
        || score.length === 0) {
      this.setState({
        score: score,
        showListPopup: true,
        selectedItem: null
      });

      if (this.props.onChange) {
        this.props.onChange(
          {
            name: this.props.name,
            element: ScoredItemWrapper.wrapValue(this.state.feedback, score)
          }
        );
      }
    }
  };

  handleKeyUp = (e) => {
    if (e.key === ARROW_DOWN_KEY || e.key === ARROW_UP_KEY || e.key === ENTER_KEY) {
      const newState = {keyEvent: {key: e.key, fingerprintTimestamp: e.timeStamp}};

      if (e.key === ARROW_DOWN_KEY && this.state.showListPopup === false) {
        newState.showListPopup = true;
      }

      this.setState(newState);
      e.stopPropagation();
    } else if (e.key === ESCAPE_KEY) {
      this.setState({
        showListPopup: false,
        keyEvent: null,
      });
      e.stopPropagation();
    } else if (e.key === TAB_KEY) {
      this.setState({
        showListPopup: false,
        keyEvent: null,
      });
    } else {
      this.setState({keyEvent: null});
    }
  };

  onValueSelect = (newValue) => {
    this.setState({
      showListPopup: false,
      feedback: newValue.feedback,
      score: newValue.score,
      selectedItem: newValue
    });

    ReactDOM.findDOMNode(this.refs.feedbackInputFieldRef).focus();

    if (this.props.onSelect) {
      this.props.onSelect(
        {
          name: this.props.name,
          element: newValue
        }
      );
    }
  };

  hideListPopup = () => {
    this.setState({showListPopup: false});
  };

  switchListPopup = () => {
    this.setState({showListPopup: !this.state.showListPopup});
  }

  render() {
    let inputGroupClassName = 'sf-form-control-combobox sf-combobox sf-widget';
    if (this.state.feedbackFocused) {
      inputGroupClassName += ' sf-state-focused';
    }

    let scoreGroupClassName = 'input-group sf-score-group';
    if (this.state.scoreFocused) {
      scoreGroupClassName += ' sf-state-focused';
    }

    let lbl;
    if (this.props.label) {
      lbl = (
          <label className='control-label'>
            <span>{this.props.label}</span>
          </label>
      );
    }

    return (
        <div>
          {lbl}
          <span className={scoreGroupClassName}>
            <input
                className='form-control sf-score-input'
                value={this.state.score}
                onChange={this.handleScoreChange}
                onFocus={this.handleScoreFocus}
                onBlur={this.handleScoreBlur}
              />
            <span className='input-group-addon sf-score-tail'>/100</span>
          </span>
          <div className='input-group sf-input-group input-group-sm sf-input-group-sm'>
            <div className={inputGroupClassName}>
              <button onClick={this.switchListPopup} tabIndex='-1' className='sf-select sf-btn'>
                <i className='sf-i sf-i-caret-down'>
                  <span className='sf-sr'>open combobox</span>
                </i>
              </button>
              <textarea
                  ref='feedbackInputFieldRef'
                  className='sf-input sf-input-area'
                  onChange={this.handleFeedbackChange}
                  onFocus={this.handleFeedbackFocus}
                  onBlur={this.handleFeedbackBlur}
                  onKeyUp={this.handleKeyUp}
                  value={this.state.feedback}
                  placeholder={this.props.placeholder}
                />
            </div>

            <ListComponent
                showListPopup={this.state.showListPopup}
                list={this.state.list}
                highlightFragment={this.state.feedback}
                forceHide={this.hideListPopup}
                keyEvent={this.state.keyEvent}
                onSelect={this.onValueSelect}
              />
          </div>
        </div>
    );
  }
}

class ListComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      list: [],
      keyboardPickedItemIndex: -1,
      highlightFragment: '',
      visible: false
    }
  }

  onShow() {
    document.body.addEventListener('click', this.handleGlobalClick);

    this.setState({keyboardPickedItemIndex: -1});
  }

  onHide() {
    document.body.removeEventListener('click', this.handleGlobalClick);
  }

  componentWillReceiveProps(newProps) {
    const newState = {};

    if (!this.state.visible && newProps.showListPopup) {
      this.onShow();
    } else if (this.state.visible && !newProps.showListPopup) {
      this.onHide();
    }
    newState.visible = newProps.showListPopup;

    const thisKeyEventTimestamp = this.state.keyEvent == null ? -1 : this.state.keyEvent.fingerprintTimestamp;

    if (newProps.keyEvent && newProps.keyEvent.fingerprintTimestamp > thisKeyEventTimestamp) {
      this.executeKeyAction(newProps.keyEvent);
      newState.keyEvent = newProps.keyEvent;
    }

    newState.list = newProps.list;
    newState.highlightFragment = newProps.highlightFragment;

    this.setState(newState);
  }

  executeKeyAction(keyEvent) {
    let selectedIndex = this.state.keyboardPickedItemIndex;

    let newIndex;
    if (keyEvent.key === ARROW_DOWN_KEY) {
      const maxIndex = this.props.list.length - 1;
      selectedIndex++;
      newIndex = selectedIndex <= maxIndex ? selectedIndex : maxIndex;

    } else if (keyEvent.key === ARROW_UP_KEY) {
      const minIndex = 0;
      selectedIndex--;
      newIndex = selectedIndex >= minIndex ? selectedIndex : minIndex;

    } else if (keyEvent.key === ENTER_KEY && this.state.keyboardPickedItemIndex >= 0) {
      this.props.onSelect(this.props.list[this.state.keyboardPickedItemIndex]);
      newIndex = -1;
    }

    if (newIndex || newIndex === 0) {
      this.setState({ keyboardPickedItemIndex: newIndex });
    }
  };

  componentWillUnmount() {
    document.body.removeEventListener('click', this.handleGlobalClick);
  }

  handleGlobalClick = (evt) => {
    const bounds = ReactDOM.findDOMNode(this.refs.listElementRef).getBoundingClientRect();

    const mX = evt.clientX;
    const mY = evt.clientY;

    if (mX < bounds.left
        || mX > bounds.right
        || mY < bounds.top
        || mY > bounds.bottom) {

      this.props.forceHide();
    }
  };

  renderList = () => {
    const onSelectHandler = this.props.onSelect;
    const keyboardPickedIndex = this.state.keyboardPickedItemIndex;
    const highlightFragment = this.state.highlightFragment;

    if (this.state.list.length === 0) {
      return [(<li key='empty' className='sf-list-empty'>-There are no items in the list-</li>)];
    } else {
      const componentsList = this.state.list.map(function(item, index) {

        let itemClassName = 'sf-list-option';
        if (index === keyboardPickedIndex) {
          itemClassName += ' sf-state-selected';
        }

        return (
            <li
                key={index}
                className={itemClassName}
                onClick={() => onSelectHandler(item)} >
              <span>
                {highlightText(item.feedback, highlightFragment)}
                {' '}
                <strong>{item.score}{'/100'}</strong>
              </span>
            </li>
        );
      });

      return componentsList;
    }
  };

  render() {
    return (
      <div className={ this.state.visible ? 'sf-popup-container' : 'hide' } >
        <div className='sf-popup sf-widget sf-combobox-panel'>
          <ul ref='listElementRef' className='sf-list sf-list-grouped'>
            {this.renderList()}
          </ul>
        </div>
      </div>
    )
  }
}

function highlightText(textValue, searchTerm) {
  const highlightedElements = [];

  const cleanTerm = searchTerm ? searchTerm.trim() : '';
  if (cleanTerm) {

    const words = textValue.split(' ');
    const searchTermWords = cleanTerm.split(' ');

    words.forEach((w, idx) =>
      {
        if (searchTermWords.indexOf(w) >= 0) {
          highlightedElements.push(<strong key={w + '_' + idx}>{w + ' '}</strong>);
        } else {
          highlightedElements.push(w + ' ');
        }
      }
    )
  } else {
    highlightedElements.push(textValue);
  }

  return highlightedElements;
};

class ScoredItemWrapper {

  constructor(uuid, feedback, score) {
    this.uuid = uuid;
    this.feedback = feedback;
    this.score = score;
    this._searchTerm = '';
  };

  getName() {
    return this.feedback;
  };

  getScore() {
    return this.score;
  };

  containsUpperCase(searchTermUpper) {
    return !searchTermUpper || this.feedback.toUpperCase().indexOf(searchTermUpper) >= 0;
  };

  static findModelObject(elementValue, list) {
    if (elementValue) {

      let element = null;
      if (typeof elementValue === 'string') {
        element = new ScoredItemWrapper(null, elementValue, -1);
      } else if (elementValue instanceof ScoredItemWrapper) {
        element = elementValue;
      } else {
        throw new Error('Illegal element value');
      }

      for (let i = 0; i < list.length; i++) {
        const listValue = list[i];
        if (listValue.uuid && element.uuid) {
          if (listValue.uuid === element.uuid) {
            return listValue;
          }
        } else if (listValue.feedback === element.feedback) {
          return listValue;
        }
      }
    }

    return null;
  }

  static wrapValue(feedback, score) {
    if (feedback instanceof ScoredItemWrapper) {
      return feedback;
    } else {
      return new ScoredItemWrapper(null, feedback, score);
    }
  };

  static unwrapValue(value) {
    if (value) {
      if (value instanceof ScoredItemWrapper) {
        return value.feedback;
      } else {
        return value;
      }
    }

    return '';
  };

};

ScoredFeedbackInput.ItemWrapper = ScoredItemWrapper;

ScoredFeedbackInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,

  value: PropTypes.shape({
    feedback: PropTypes.string.isRequired,
    score: PropTypes.string.isRequired
  }),

  placeholder: PropTypes.string,
  autoFocus: PropTypes.bool,

  list: PropTypes.array.isRequired,

  onChange: PropTypes.func,
  onSelect: PropTypes.func
}

export default ScoredFeedbackInput;
