import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import registerServiceWorker from './registerServiceWorker';

import './index.evalorm.css';

import configureStore from './evaluation-form/store.evalorm';
import { EvaluationFormAsyncActions } from './evaluation-form/actions.evalorm';
import evaluationFormManagerApp from './evaluation-form/reducers.evalorm';

import EvaluationFormManager from './evaluation-form/EvaluationFormManager.evalorm';
import EvaluationFormPages from './evaluation-form/EvaluationFormPages.evalorm';


const store = configureStore(
  evaluationFormManagerApp,
  {/* predefined state, if needed*/
    currentPage: EvaluationFormPages.EVALUATION_FORMS_LIST
  }
);

// initial data fetching
store.dispatch(EvaluationFormAsyncActions.fetchEFRecordsList());

ReactDOM.render(
  <Provider store={store}>
    <EvaluationFormManager />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
