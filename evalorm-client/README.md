# Evalorm.io front-end

### General ###

* Experiment project for evaluation forms - client app part
* Version 0.0.1

### How do I get set up? ###

* This is client side that is based on ReactJs
* You need installing NPM 5.4.1 and then execute `/>npm install` within the root folder of evalorm-client project
* To run the project run `/>npm start` within the root folder of evalorm-client project
* Find your application on [localhost:3000](http://localhost:3000)

### Contribution guidelines ###

* Write tests
* Do code reviews

### Who do I talk to? ###

* mak.spectrum@gmail.com